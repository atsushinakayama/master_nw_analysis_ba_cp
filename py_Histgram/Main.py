import os
import sys
from module.func import *

'''
inputPath  = os.path.join(
			os.path.dirname(__file__), '..', 'Results'
			)
'''
inputPath = "./Results/"
#output Histgram ([ba,cp],alivebank,totalloss)
wbba1 = xlrd.open_workbook(inputPath+"workbookBApt1.xls")
wbcp1 = xlrd.open_workbook(inputPath+"workbookCPpt1.xls")
wbba2 = xlrd.open_workbook(inputPath+"workbookBApt2.xls")
wbcp2 = xlrd.open_workbook(inputPath+"workbookCPpt2.xls")
wbba3 = xlrd.open_workbook(inputPath+"workbookBApt3.xls")
wbcp3 = xlrd.open_workbook(inputPath+"workbookCPpt3.xls")
create_Histgram_alive(wbba1,wbcp1,"pt1")
create_Histgram_totalloss(wbba1,wbcp1,"pt1")
create_Histgram_alive(wbba2,wbcp2,"pt2")
create_Histgram_totalloss(wbba2,wbcp2,"pt2")
create_Histgram_alive(wbba3,wbcp3,"pt3")
create_Histgram_totalloss(wbba3,wbcp3,"pt3")
sys.exit()