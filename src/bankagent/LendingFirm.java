package bankagent;

public class LendingFirm implements Cloneable{

	public int firmid,tau;
	public double lfprice;
	public double interest;

	public int taucounter;

	public double installment;//割賦
	public double balance;//割賦全額
	public double repayprincipal;//貸出元本
	public double interestpayment;//t期に返済される金利
	public double remainingprincipal;//残りの貸出元本

	public LendingFirm(int firmid, double lfprice, double interest, int tau) {
		// TODO Auto-generated constructor stub
		this.firmid=firmid;
		this.lfprice=lfprice;
		this.interest=interest;
		this.tau=tau;
		this.taucounter=0;

		setInst();
	}

	private void setInst() {
		// TODO 自動生成されたメソッド・スタブ
		balance=(1+interest)*lfprice;
		installment=balance/tau;
		repayprincipal=lfprice;
		remainingprincipal=lfprice;
		interestpayment=installment-(lfprice/tau);
	}

	public void updateList() {
		// TODO Auto-generated method stub
		balance-=installment;
		remainingprincipal-=lfprice/tau;
		taucounter++;

	}

	public int getTaucoungter() {
		// TODO 自動生成されたメソッド・スタブ
		return taucounter;
	}

	public int getTau() {
		// TODO 自動生成されたメソッド・スタブ
		return tau;
	}

	public double getRemainingPrincipal() {
		// TODO 自動生成されたメソッド・スタブ
		return remainingprincipal;
	}

	public LendingFirm clone() {
		try {
			LendingFirm lf=(LendingFirm) super.clone();
			return lf;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}



}
