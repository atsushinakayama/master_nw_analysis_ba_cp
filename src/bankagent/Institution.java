package bankagent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import asset.Asset;

public class Institution implements Cloneable {

	//仮想ネットワーク作成のためのコンストラクタ
	public Institution(int num, int p, int q) {
		id = num;
		indeg = p;
		outdeg = q;
		deg = p + q;
	}

	public Institution (int id) {
		this.id=id;
		this.CAR=10000;//fucking破綻処理を防ぐため
	}

	//対称型コンストラクタ
	public Institution(int i, double a0) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = i;
		this.A0 = a0;
		n = A0;
		buffer = A0;
		d = 0;
	}

	public int id;//金融機関のID
	public int indeg, outdeg, deg;

	public double A0;//

	public String state;//現存か破綻Alive Dead
	public String rank;
	public String brfactor;//破綻要因(CAR or Buffer or Both)

	public double w1, w2;//ネットワーク重み
	public ArrayList<Integer> cpbrin = new ArrayList<Integer>();//取引枝の作成(counterpartybranch)
	public ArrayList<Integer> cpbrout = new ArrayList<Integer>();//取引枝の作成(counterpartybranch)

	public double initCAR;//初期設定自己資本比率
	public int initassetnum;//初期外部資産保持数
	public double CAR;//自己資本比率
	public double totalassets;//総資産
	public double totalinvest;//総投資
	public double rasize;//relative assetsize
	public double n;//自己資本金
	public double d;//預金等他人資本
	public double lf;//企業貸付額
	public double lft;//ステップtに企業に貸付をする予定である金額
	public double lb;//短期インターバンク貸付金
	public double bb;//短期インターバンク借入金
	public double liqA;//市場性資産
	public double realliqA;//正味価格
	public double buffer;//余剰金
	public double shortage;//不足金
	public double gap;//step tでの資金ギャップ

	public double pailf;//企業からの金利収入
	public double pailb;//銀行からの金利収入
	public double paibb;//銀行に支払う金利
	public double pai;//ステップtでの利益
	public double badfloan;//企業貸付の焦げ付き
	public double badbloan;//銀行貸し付けの焦げ付き
	public double grossbadloan;//総貸出損失
	public double expense;//経費
	public double divident;//配当
	public double residual;//残余

	public double S;//1期の取引貸出可能額
	public double SS;//thirdcredit市場貸出し制限額
	public double forcelev;//目標レバレッジ
	public double liqArate;//市場性資産保持率
	public double ddt;//t期における受け入れるべき他人資本
	public double liqAplust;//t期における追加投資LiqAsset
	public double liqAprofit;//市場性資産収益

	public double rddt;//t期における売却（減少）すべき資本
	public double rbuf;//売却留保金

	public double ibrate;//ib貸出比率
	public double ibable;//ib貸出可能額

	//自己組織型
	public int levnownum;//レバレッジレベルナンバー
	public double nextlev;
	public double sigmat;
	public double counter;

	public int clientnum;//顧客数
	public int initclientnum;//初期顧客数
	public ArrayList<Integer> ClientNum = new ArrayList<Integer>();

	//InterBank Market
	public ArrayList<Integer> Inid = new ArrayList<Integer>();
	public ArrayList<Integer> Outid = new ArrayList<Integer>();
	public double ibborrowrate, ibloanrate;
	public int degwin, degwout;//ib枝の重み
	public int sumdegw;
	public ArrayList<Integer> Indegw = new ArrayList<Integer>();
	public ArrayList<Integer> Outdegw = new ArrayList<Integer>();

	//regulation
	public double CARD;//最低要求自己資本比率

	//NWCoef
	public double pr;
	public double difpr;
	public ArrayList<Double> prlist = new ArrayList<Double>();
	public ArrayList<Double> difprlist = new ArrayList<Double>();

	public double dr,difdr;
	public ArrayList<Double> drlist = new ArrayList<Double>();
	public ArrayList<Double> difdrlist = new ArrayList<Double>();


	//ExAsset
	public ArrayList<AssetList> Exasset = new ArrayList<AssetList>();

	//longterm
	//ArrayList<Leverage> levlist=new ArrayList<Leverage>();
	//ArrayList<LevProbably> list=new ArrayList<LevProbably>();
	public ArrayList<Double> levlevellist = new ArrayList<Double>();

	public ArrayList<Double> pailist = new ArrayList<Double>();
	public ArrayList<Double> lftlist = new ArrayList<Double>();
	public ArrayList<Double> nlist = new ArrayList<Double>();
	public ArrayList<Double> CARlist = new ArrayList<Double>();
	public ArrayList<Double> bufferlist = new ArrayList<Double>();
	public ArrayList<Double> Residual = new ArrayList<Double>();

	public ArrayList<LInterest> rf = new ArrayList<LInterest>();//全Firm貸出金利
	public ArrayList<DemandOrderList> DOL = new ArrayList<DemandOrderList>();//企業からの要望リスト
	public ArrayList<LendingFirm> LFlist = new ArrayList<LendingFirm>();//企業貸出リスト

	//shortterm
	public ArrayList<Double> sCARlist=new ArrayList<Double>();

	//長期IB市場
	public ArrayList<IBLoan> LBlist = new ArrayList<IBLoan>();//IB貸出リスト
	public ArrayList<IBBorrow> BBlist = new ArrayList<IBBorrow>();//IB借入リスト
	public ArrayList<IBBorrow> newBBlist = new ArrayList<IBBorrow>();//t期のIB借入リスト
	public ArrayList<IBLoan> newLBlist = new ArrayList<IBLoan>();//t期のIB貸出リスト
	public ArrayList<IBList> IBlist = new ArrayList<IBList>();//IB需要リスト

	//短期IB市場
	public ArrayList<IBShort> ibsloan = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsorder = new ArrayList<IBShort>();//オーダーリスト
	public ArrayList<IBShort> ibsrecievedorder = new ArrayList<IBShort>();//オーダーリスト

	public ArrayList<Integer> ibsrejectedbank = new ArrayList<Integer>();//オーダーを拒否された銀行リスト
	public ArrayList<Integer> ibsconnectedbank = new ArrayList<Integer>();//借入を行っている銀行リスト
	public ArrayList<Integer> ibsnotconnectedbank = new ArrayList<Integer>();//借入を行っていない銀行リスト

	public ArrayList<Double> Slist = new ArrayList<Double>();//タームごと貸し出し可能額
	public ArrayList<Double> goallevt = new ArrayList<Double>();//タイムステップごと目標レバレッジ
	public ArrayList<Double> actlevt = new ArrayList<Double>();//タイムステップごと実際のレバレッジ

///////CloneList(毎ターン消去しないもの)

	//public ArrayList<LInterest> rf1 = new ArrayList<LInterest>();//全Firm貸出金利
	//public ArrayList<DemandOrderList> DOL1 = new ArrayList<DemandOrderList>();//企業からの要望リスト
	public ArrayList<LendingFirm> LFlist1 = new ArrayList<LendingFirm>();//企業貸出リスト
	//public ArrayList<IBLoan> LBlist1 = new ArrayList<IBLoan>();//IB貸出リスト
	//public ArrayList<IBBorrow> BBlist1 = new ArrayList<IBBorrow>();//IB借入リスト
	public ArrayList<IBShort> ibsloan1 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow1 = new ArrayList<IBShort>();//貸出リスト
	//public ArrayList<IBShort> ibsorder1 = new ArrayList<IBShort>();//オーダーリスト
	//public ArrayList<IBShort> ibsrecievedorder1 = new ArrayList<IBShort>();//オーダーリスト
	public ArrayList<AssetList> Exasset1 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist2 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan2 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow2 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset2 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist3 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan3 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow3 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset3 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist4 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan4 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow4 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset4 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist5 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan5 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow5 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset5 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist6 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan6 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow6 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset6 = new ArrayList<AssetList>();

	public ArrayList<LendingFirm> LFlist7 = new ArrayList<LendingFirm>();//企業貸出リスト
	public ArrayList<IBShort> ibsloan7 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<IBShort> ibsborrow7 = new ArrayList<IBShort>();//貸出リスト
	public ArrayList<AssetList> Exasset7 = new ArrayList<AssetList>();

///////////////////////////////////

	//set NWCoef
	//debtrank
	public void setDebtRank(double r) {
		this.dr = r;
		if(drlist.size()!=0) {
			this.difdr = Math.abs(drlist.get(drlist.size()-1)-dr);
		}else {
			this.difdr = 0;
		}
		drlist.add(this.dr);
		difdrlist.add(this.difdr);
	}

	//pagerank
	public void setPageRank(double pr) {
		this.pr = pr;
		if(prlist.size()!=0) {
			this.difpr = Math.abs(prlist.get(prlist.size()-1)-pr);
		}else {
			this.difpr = 0;
		}
		prlist.add(this.pr);
		difprlist.add(this.difpr);
	}

	//set regulation
	public void setCARD(double mincard, double maxcard) {
		//初期carのマイナス差分をcardとする．
		CARD = mincard + (maxcard - mincard) * Math.random();
	}

	//after longterm to shortterm
	public void setInitIBBS() {
		totalassets = bb + d + n;
		totalinvest = lb + lf + liqA + buffer;
	}

	public void addExAsset(ArrayList<Asset> asset, double dif) {
		liqA += dif;
		realliqA += dif;
		Exasset.stream().forEach(s -> {
			s.addAmount(dif / Exasset.size());
			for (Asset as : asset) {
				if (as.id == s.id) {
					as.setTotalAmount(dif / Exasset.size());
				}
			}
			;
		});
	}

	public void cutExasset(ArrayList<Asset> asset, double dif) {
		liqA -= dif;
		realliqA -= dif;
		Exasset.stream().forEach(s -> {
			s.cutAmount(dif / Exasset.size());
			for (Asset as : asset) {
				if (as.id == s.id) {
					as.cutTotalAmount(dif / Exasset.size());
				}
			}
			;
		});
	}

	public void setNetWorth() {
		n = totalassets * CAR;
		d -= n;
		setInitIBBS();
	}

	public void adjustNetWorth() {
		double am = n+d;
		n= totalassets*CAR;
		d = am-n;
		setInitIBBS();
	}

	public void setSumdegw() {
		sumdegw = Indegw.size() + Outdegw.size();
	}

	public void setIBrate() {
		ibborrowrate = bb / totalassets;
		ibloanrate = lb / totalassets;
	}

	public void setIBIndividualrate() {
		cpbrin.stream().forEach(s -> degwin += s);
		cpbrout.stream().forEach(s -> degwout += s);
		//cpbrin.stream().forEach(s->Indegw.add((double) (s/degwin)));
		//cpbrout.stream().forEach(s->Outdegw.add((double) (s/degwout)));

	}

	//during shortterm
	public void IBSInit(int banknode) {
		//money=0のリストを消去
		Iterator<IBShort> ibsl = ibsloan.iterator();
		while (ibsl.hasNext()) {
			IBShort il = ibsl.next();
			if (il.money == 0) {
				ibsl.remove();
			}
		}
		Iterator<IBShort> ibsb = ibsborrow.iterator();
		while (ibsb.hasNext()) {
			IBShort ib = ibsb.next();
			if (ib.money == 0) {
				ibsb.remove();
			}
		}

		Iterator<AssetList> as = Exasset.iterator();
		while (as.hasNext()) {
			AssetList aslist = as.next();
			if (aslist.amount == 0) {
				as.remove();
			}
		}

		ibsconnectedbank.clear();
		for (IBShort ibb : ibsborrow) {
			ibsconnectedbank.add(ibb.id);
		}
		for(IBShort ibl:ibsloan) {
			if(!ibsconnectedbank.contains(ibl.id)) {
				ibsconnectedbank.add(ibl.id);
			}
		}

		//取引銀行の整理
		ibsnotconnectedbank.clear();
		for (int i = 1; i <= banknode; i++) {
			if (!ibsconnectedbank.contains(i) && i != id) {
				ibsnotconnectedbank.add(i);
			}
		}

		//shorttermlist格納
		sCARlist.add(CAR);

	}

	public void depOut(double depoutflow) {
		double outflow = d * depoutflow;
		d -= outflow;
	}

	public void asFluctuation(ArrayList<Asset> asset) {

		for (Asset as : asset) {
			for (AssetList aslist : Exasset) {
				if (as.id == aslist.id) {
					aslist.setFlucrate(as.flucrate);
					aslist.setCuramount();
				}
			}
		}

	}

	public void sellAsset() {
		//一番マイナスが大きい(ランダムに選んでもよいかも)外部資産を目標？まで売却
		//bufferが足りない場合に売却
		//
		/*ToDo
		 * selledamountとかはここで代入
		 * 規制を考慮(LCR,NSFR)
		 * cardでの売却はlongtermで
		 */
		if (buffer < 0) {
			int minindex = 0;
			double min = 0;
			for (AssetList aslist : Exasset) {
				if (min > aslist.def)
					minindex = aslist.id;
			}
			for (AssetList aslist : Exasset) {
				if (aslist.id == minindex) {
					if (-buffer < aslist.curvalue) {
						aslist.setPartOfSellAmount(buffer);
						buffer = 0;
					} else {
						buffer += aslist.curvalue;
						aslist.setFullSellAmount();
					}
				}
			}
		}

	}

	public void buyAsset() {
		//目標保持数になるよう購入
		//目標保持数の場合はすべてを保持分割合だけ購入
		/*ToDo
		 * 規制を考慮(LCR,NSFR)
		 */

	}

	public void setIBLoanWithCAR(ArrayList<Institution> bank) {
		//set ibloan for CAR
		for(IBShort ibl:ibsloan) {
			for(Institution b:bank) {
				if(ibl.id==b.id) {
					ibl.CAR=b.CAR;
				}
			}
		}
	}



	public void setIBOrder(String state) {

		//オーダー先はランダムに
		if (state == "connected") {
			Collections.shuffle(ibsborrow);
			if (ibsborrow.size() > 0) {
				/*
				IBShortOrder order = new IBShortOrder(ibsborrow.get(0).id, buffer);
				ibsorder.add(order);
				*/
				for (IBShort ib : ibsborrow) {
					if (!ibsrejectedbank.contains(ib.id)) {
						IBShort order = new IBShort(ib.id, shortage);
						ibsorder.add(order);
						break;
					}
				}
			}
		}

		if (state == "notconnected") {
			Collections.shuffle(ibsnotconnectedbank);
			if (ibsnotconnectedbank.size() > 0) {
				for (Integer ib : ibsnotconnectedbank) {
					if (!ibsrejectedbank.contains(ib)) {
						IBShort order = new IBShort(ib, shortage);
						ibsorder.add(order);
						break;
					}
				}
			}
		}

	}

	public void setIBrecievedOrder(int id, double money, double car) {
		/*
		IBShortrecievedOrder rorder = new IBShortrecievedOrder(id, money);
		ibsrecievedorder.add(rorder);
		*/
		IBShort rorder = new IBShort(id, money, car);
		ibsrecievedorder.add(rorder);

	}

	public void setIBresponseOrder() {

		/*
		 * 何らかの貸出判断(ibr.money=0にする)
		 * ここでbufferを減らしておく
		 */
		Collections.shuffle(ibsrecievedorder);
		for (IBShort ibr : ibsrecievedorder) {
			if (buffer == 0) {
				ibr.money = 0;
			} else {
				if (ibr.CAR > CARD) {
					if (ibr.money > buffer) {
						ibr.money = buffer;
						buffer = 0;
					} else {
						buffer -= ibr.money;
					}
				}else {
					ibr.money=0;
					//System.out.println(id+"銀行IBOrder拒否 CARD"+CARD+"   "+ibr.id+"銀行CAR"+ibr.CAR);
				}
			}

		}

	}

	public void addibsloan(int bankid, double money) {

		ibsloan.stream().filter(s -> s.id == bankid)
				.forEach(s -> s.addloanmoney(money));

		//新規取引先の場合
		if (!ibsconnectedbank.contains(bankid)) {
			IBShort ibs = new IBShort(bankid, money);
			ibsloan.add(ibs);
		}
	}

	public void addibsborrow(int bankid, double money) {

		ibsborrow.stream().filter(s -> s.id == bankid)
				.forEach(s -> s.addborrowmoney(money));
		//新規取引先の場合
		if (!ibsconnectedbank.contains(bankid)) {
			IBShort ibs = new IBShort(bankid, money);
			ibsborrow.add(ibs);
		}
		shortage -= money;
	}

	public void setIBShortBS() {

		lb = 0;
		bb = 0;
		liqA = 0;
		realliqA = 0;
		buffer = 0;
		double nowtotalasset = 0, nowtotalinvest = 0;

		ibsborrow.stream().forEach(s -> bb += s.money);
		ibsloan.stream().forEach(s -> lb += s.money);
		Exasset.stream().forEach(s -> {
			liqA += s.amount;
			realliqA += s.curvalue;
		});
		nowtotalasset = bb + d + n;
		nowtotalinvest = liqA + lf + lb;
		buffer = nowtotalasset - nowtotalinvest;
		if (buffer < 0) {
			shortage = (-buffer);
			buffer = 0;
		}

		totalassets = bb + d + n;
		totalinvest = liqA + lf + lb + buffer;
		CAR = (n + (realliqA - liqA)) / (bb + d + n);

	}

	public void removeIBOrder() {
		ibsorder.clear();
		ibsrecievedorder.clear();
	}

	public void brProcessing(double brCAR) {
		state = "Dead";
		if (shortage > 0.00001 && CAR < brCAR) {
			brfactor = "Both";
		} else {
			if (shortage > 0.00001)
				brfactor = "Buffer";
			if (CAR < brCAR)
				brfactor = "CAR";
		}
		//System.out.println("dead id:"+id+" brfactor:"+brfactor);
	}

	//after shorterm brprocessing
	public void brProcessing1(double brCAR) {
		state = "Dead";
		if (shortage > 0.000001 && CAR < brCAR) {
			brfactor = "Both";
		} else {
			if (shortage > 0.000001)
				brfactor = "Buffer";
			if (CAR < brCAR)
				brfactor = "CAR";
		}
		//System.out.println("cmdead id:"+id+" brfactor:"+brfactor);
	}

	public double setIBShock(int id, double grossbadloan) {
		for (IBShort loan : ibsloan) {
			if (loan.id == id) {
				//System.out.println("IBSHOCK :"+loan.money+"  by"+loan.id+"銀行");
				n -= loan.money;
				badbloan -= loan.money;
				grossbadloan -= loan.money;
				loan.money = 0;
			}
		}
		return grossbadloan;
	}

	public void setBufferShortage() {
		if (shortage > 0.001) {
			if (buffer > 0.001) {
				if (buffer > shortage) {
					buffer -= shortage;
					shortage = 0;
				} else {
					shortage -= buffer;
					buffer = 0;
				}
			} else {
				shortage += (-buffer);
				buffer = 0;
			}
		}
	}

	public void setBS1(String expstate) {
		lf = 0;
		LFlist.stream().forEach(s -> lf += s.getRemainingPrincipal());
		totalassets = d + bb + n;
		totalinvest = liqA + lf + buffer + lb;
		if(expstate!="Build")CAR=(n+(liqA-realliqA))/totalassets;

	}

	public double getTotalassets() {
		return totalassets;
	}

	public void setBS() {

		//外部資金(d)の調達
		ddt = (n / nextlev) - totalassets;
		if (ddt > 0.1) {
			d += ddt;
			totalassets += ddt;
			liqAplust = totalassets * liqArate - liqA;
			if (liqAplust > ddt) {
				liqAplust = ddt;
				liqA += liqAplust;
			} else {
				liqA += liqAplust;
			}
			if (ddt - liqAplust > 0) {
				buffer += ddt - liqAplust;
			}
		} else {

			//資産の縮小(buffer分だけ)
			if (ddt < -0.1) {
				if (-ddt < buffer) {
					totalassets += ddt;
					d += ddt;
					buffer += ddt;

				} else {
					totalassets -= buffer;
					d += buffer;
					buffer = 0;
				}
			}
			/*
			//資産の縮小(流動性資産も割合を保つように)
			if(ddt<-0.1) {
				if(-ddt<buffer) {
					totalassets+=ddt;
					d+=ddt;
					buffer+=ddt;

				}else {
					totalassets-=buffer;

					d+=buffer;
					buffer=0;
				}
			}*/

		}

	}

	public void addInterest(int firmid, double r) {

		LInterest firminterest = new LInterest(firmid, r);
		rf.add(firminterest);
	}

	public void addDOL(int firmid, double interest, double l, double ps, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		DemandOrderList dol = new DemandOrderList(firmid, interest, l, ps, tau);
		DOL.add(dol);

	}

	public void addIBList(double ibdemand, double r, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		IBList iblist = new IBList(ibdemand, r, tau);//(IB需要金、金利、期限）
		IBlist.add(iblist);

	}

	public void addLBList(int ibloanid, double ibloanmoney, double r, int tau) {

		IBLoan ibloan = new IBLoan(ibloanid, ibloanmoney, r, tau);
		LBlist.add(ibloan);
	}

	public void addnewLBList(int ibloanid, double ibloanmoney, double r, int tau) {

		IBLoan ibloan = new IBLoan(ibloanid, ibloanmoney, r, tau);
		newLBlist.add(ibloan);
	}

	public void addBBList(int IBborrowid, double ibborrowmoney, double r, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		IBBorrow ibborrow = new IBBorrow(IBborrowid, ibborrowmoney, r, tau);
		BBlist.add(ibborrow);
	}

	public void addnewBBList(int IBborrowid, double ibborrowmoney, double r, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		IBBorrow ibborrow = new IBBorrow(IBborrowid, ibborrowmoney, r, tau);
		newBBlist.add(ibborrow);
	}

	public void addLF(int firmid, double lfprice, double interest, int tau) {
		// TODO Auto-generated method stub
		LendingFirm lf = new LendingFirm(firmid, lfprice, interest, tau);
		LFlist.add(lf);
	}

	public void addnewLFlist(int firmid, double lfprice, double interest, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		LendingFirm lf = new LendingFirm(firmid, lfprice, interest, tau);
		//newLFlist.add(lf);
	}

	public double getTotalAssets() {
		// TODO 自動生成されたメソッド・スタブ
		totalassets = bb + d + n;
		return totalassets;
	}

	public double getTotalInvest() {
		// TODO 自動生成されたメソッド・スタブ
		totalinvest = lb + lf + buffer + liqA;
		return totalinvest;
	}

	public void setCAR() {
		// TODO Auto-generated method stub
		CAR = n / totalassets;
	}

	public void updateLFlist(int LFlistnumber) {
		// TODO Auto-generated method stub
		LFlist.get(LFlistnumber).updateList();

	}

	public int getTaucounter(int LFlistnumber) {
		// TODO 自動生成されたメソッド・スタブ
		return LFlist.get(LFlistnumber).getTaucoungter();
	}

	public int getTau(int LFlistnumber) {
		// TODO 自動生成されたメソッド・スタブ
		return LFlist.get(LFlistnumber).getTau();
	}

	public double getInst(int bankj) {
		// TODO Auto-generated method stub
		return BBlist.get(bankj).getInstallment();
	}

	public double getPrincipal(int bankj) {
		// TODO Auto-generated method stub
		return BBlist.get(bankj).getPrincipal();
	}

	public int getIBBTaucounter(int bankj) {
		// TODO Auto-generated method stub
		return BBlist.get(bankj).getTaucounter();
	}

	public int getIBBTau(int bankj) {
		// TODO Auto-generated method stub
		return BBlist.get(bankj).getTau();
	}

	public int getIBLTaucounter(int bankj) {
		// TODO Auto-generated method stub
		return LBlist.get(bankj).getTaucounter();
	}

	public int getIBLTau(int bankj) {
		// TODO Auto-generated method stub
		return LBlist.get(bankj).getTau();
	}

	public void updateLBlist(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		LBlist.get(bankj).updatelist();
	}

	public void updateBBlist(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		BBlist.get(bankj).updatelist();
	}

	public double getPai(double liqAr) {
		// TODO 自動生成されたメソッド・スタブ
		pai = pailf + pailb + liqAprofit - badfloan - badfloan - paibb - expense - divident;
		return pai;
	}

	public void setLongTermProfit(double liqAr) {
		// TODO 自動生成されたメソッド・スタブ
		pai = pailf + pailb + liqAr * liqA - badfloan - badfloan - paibb - expense - divident;
	}

	public double getRemainingPrincipal(int firmj) {
		// TODO 自動生成されたメソッド・スタブ
		return LFlist.get(firmj).getRemainingPrincipal();
	}

	public double getIBRemainingPrincipal(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		return LBlist.get(bankj).getIBLRemainingPrincipal();
	}

	public double getCAR() {
		// TODO 自動生成されたメソッド・スタブ
		return CAR;
	}

	public void Init() {
		// TODO 自動生成されたメソッド・スタブ
		lft = 0;
		pai = 0;
		pailf = 0;
		pailb = 0;
		paibb = 0;
		badfloan = 0;
		badbloan = 0;
		expense = 0;
		divident = 0;
		ddt = 0;
		liqAplust = 0;
		liqAprofit = 0;
		residual = 0;
		rddt = 0;
		sigmat = 0;
		counter = 0;
		clientnum = 0;
	}

	public void updateList() {
		// TODO 自動生成されたメソッド・スタブ
		pailist.add(pai);
		nlist.add(n);
		CARlist.add(CAR);
		bufferlist.add(buffer);
	}

	public void updateLevlist() {
		/*
				levlist.stream().forEach(s->{s.range="No";
				s.finalrange="No";
				s.adjacent="No";
				s.pl=0;
				});
				list.clear();
		*/
	}

	public void updateCMList() {
		// TODO 自動生成されたメソッド・スタブ

		//顧客数の計算(newBB,BB)
		ArrayList<Integer> client = new ArrayList<Integer>();

		/*
		newLFlist.stream().filter(s -> !client.contains(s.firmid))
				.forEach(s -> client.add(s.firmid));*/
		LFlist.stream().filter(s -> !client.contains(s.firmid))
				.forEach(s -> client.add(s.firmid));
		clientnum = client.size();
		ClientNum.add(clientnum);

		//CMリストの消去
		rf.clear();
		DOL.clear();
		IBlist.clear();
	}

	public double getExpense(double dr) {
		// TODO Auto-generated method stub
		expense = d * dr;
		if (id == 1)
			System.out.println(d);
		return expense;
	}

	public void setDepoOut(double dr) {
		expense = d * dr;
		d -= expense;
		buffer -= expense;
	}

	public void setPayDiv(double div) {
		if(pai>0) {
		n -= pai*div;
		buffer -= pai*div;
		pai -= pai*div;
		}
	}



	public void setLiqAProfit(double liqAr) {
		liqAprofit = liqA * liqAr;
		d += liqAprofit;
		buffer += liqAprofit;

	}

	public double getDiv(double div) {
		// TODO 自動生成されたメソッド・スタブ
		if (pai > 0) {
			divident = pai * div;
		} else {
			div = 0;
		}
		return divident;
	}


	public double getBuffer() {
		// TODO 自動生成されたメソッド・スタブ
		return bb + n + d - (lf + lb);
	}

	public void setInitForceLev() {
		initCAR = CAR;
	}

	public void setForceLev() {
		// TODO 自動生成されたメソッド・スタブ
		ddt = (n / forcelev) - totalassets;
		if (ddt > 0.1) {
			//資産増強
			d += ddt;
			totalassets += ddt;
			liqAplust = totalassets * liqArate - liqA;
			if (liqAplust > ddt) {
				liqAplust = ddt;
				liqA += liqAplust;
				realliqA += liqAplust;
			} else {
				liqA += liqAplust;
				realliqA += liqAplust;
			}
			if (ddt - liqAplust > 0) {
				buffer += ddt - liqAplust;
			}
		}
	}

	public void setRemainCAR() {
		// TODO 自動生成されたメソッド・スタブ
		rddt = totalassets - (n / initCAR);
		/*
		if(rddt<-0.1) {
			if(-rddt<buffer) {
				totalassets+=rddt;
				d+=rddt;
				buffer+=rddt;

			}else

			{
				//資産縮小(bufferｍのみ)
				totalassets-=buffer;
				d+=buffer;
				buffer=0;
			}

		}
		*/

		//LiqA割合を維持しつつ縮小
		if (rddt > 0.001) {
			double sellliqA = 0;

			totalassets -= rddt;
			sellliqA = liqA - totalassets * liqArate;
			d -= rddt;
			if (buffer - (rddt - sellliqA) > 0) {
				buffer -= (rddt - sellliqA);
				liqA = totalassets * liqArate;
			} else {
				liqA = totalassets * liqArate - (rddt - sellliqA - buffer);
				realliqA = totalassets * liqArate - (rddt - sellliqA - buffer);
				buffer = 0;
			}
		}

		//liqArateを維持できるバッファーになった場合；
		double liqAp = totalassets * liqArate - liqA;
		if (buffer > 0.001) {
			if (liqAp > 0.001) {
				if (buffer > liqAp) {
					liqA += liqAp;
					realliqA += liqAp;
					buffer -= liqAp;
				} else {
					liqA += buffer;
					realliqA += buffer;
					buffer = 0;
				}
			}
		}

	}

	public void setIBable(double bscallrate) {
		// TODO 自動生成されたメソッド・スタブ

		ibrate = bscallrate;
		ibable = totalassets * ibrate;
	}

	public double getLiqAssetProfit(double liqAr) {
		// TODO Auto-generated method stub
		liqAprofit = liqA * liqAr;
		return liqAprofit;
	}

	public void BufferProcessing() {
		// TODO 自動生成されたメソッド・スタブ

		d -= buffer;
		buffer = 0;
		totalassets = getTotalAssets();
		liqA = totalassets * liqArate;
		totalinvest = getTotalInvest();
	}

	/*
		public Institution clone() {
			Institution b=null;
			try {
				b=(Institution)super.clone();
				b=this.clone();
			}catch (Exception e) {
				e.printStackTrace();
			}
			return b;
		}
		*/

	public void setCloneList() {
		LFlist.stream().forEach(s -> LFlist1.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan1.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow1.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset1.add(s.clone()));
	}

	public void addCloneList() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist1.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan1.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow1.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset1.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist1.clear();
		ibsloan1.clear();
		ibsborrow1.clear();
		Exasset1.clear();
	}

	public void setCloneList1() {
		LFlist.stream().forEach(s -> LFlist2.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan2.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow2.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset2.add(s.clone()));
	}

	public void addCloneList1() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist2.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan2.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow2.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset2.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist2.clear();
		ibsloan2.clear();
		ibsborrow2.clear();
		Exasset2.clear();
	}

	public void setCloneList2() {
		LFlist.stream().forEach(s -> LFlist3.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan3.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow3.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset3.add(s.clone()));
	}

	public void addCloneList2() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist3.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan3.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow3.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset3.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist3.clear();
		ibsloan3.clear();
		ibsborrow3.clear();
		Exasset3.clear();
	}

	public void setCloneList3() {
		LFlist.stream().forEach(s -> LFlist4.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan4.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow4.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset4.add(s.clone()));
	}

	public void addCloneList3() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist4.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan4.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow4.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset4.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist4.clear();
		ibsloan4.clear();
		ibsborrow4.clear();
		Exasset4.clear();
	}

	public void setCloneList4() {
		LFlist.stream().forEach(s -> LFlist5.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan5.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow5.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset5.add(s.clone()));
	}

	public void addCloneList4() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist5.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan5.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow5.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset5.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist5.clear();
		ibsloan5.clear();
		ibsborrow5.clear();
		Exasset5.clear();
	}

	public void setCloneList5() {
		LFlist.stream().forEach(s -> LFlist6.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan6.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow6.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset6.add(s.clone()));
	}

	public void addCloneList5() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist6.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan6.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow6.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset6.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist6.clear();
		ibsloan6.clear();
		ibsborrow6.clear();
		Exasset6.clear();
	}
	public void setCloneList6() {
		LFlist.stream().forEach(s -> LFlist7.add(s.clone()));
		ibsloan.stream().forEach(s -> ibsloan7.add(s.clone()));
		ibsborrow.stream().forEach(s -> ibsborrow7.add(s.clone()));
		Exasset.stream().forEach(s -> Exasset7.add(s.clone()));
	}

	public void addCloneList6() {
		LFlist.clear();
		ibsloan.clear();
		ibsborrow.clear();
		Exasset.clear();
		LFlist7.stream().forEach(s -> LFlist.add(s.clone()));
		ibsloan7.stream().forEach(s -> ibsloan.add(s.clone()));
		ibsborrow7.stream().forEach(s -> ibsborrow.add(s.clone()));
		Exasset7.stream().forEach(s -> Exasset.add(s.clone()));
		LFlist7.clear();
		ibsloan7.clear();
		ibsborrow7.clear();
		Exasset7.clear();
	}

	public Institution clone() {
		try {
			Institution b = (Institution) super.clone();
			b.Inid = new ArrayList<Integer>(Inid);
			b.Outid = new ArrayList<Integer>(Outid);
			b.cpbrin = new ArrayList<Integer>(cpbrin);
			b.cpbrout = new ArrayList<Integer>(cpbrout);
			b.ClientNum = new ArrayList<Integer>(ClientNum);
			b.Indegw = new ArrayList<Integer>(Indegw);
			b.Outdegw = new ArrayList<Integer>(Outdegw);
			b.Exasset = new ArrayList<AssetList>(Exasset);
			b.pailist = new ArrayList<Double>(pailist);
			b.lftlist = new ArrayList<Double>(lftlist);
			b.nlist = new ArrayList<Double>(nlist);
			b.CARlist = new ArrayList<Double>(CARlist);
			b.bufferlist = new ArrayList<Double>(bufferlist);
			b.Residual = new ArrayList<Double>(Residual);
			b.rf = new ArrayList<LInterest>(rf);
			b.DOL = new ArrayList<DemandOrderList>(DOL);
			b.LFlist = new ArrayList<LendingFirm>(LFlist);
			b.ibsloan = new ArrayList<IBShort>(ibsloan);
			b.ibsborrow = new ArrayList<IBShort>(ibsborrow);
			b.ibsorder = new ArrayList<IBShort>(ibsorder);
			b.ibsrecievedorder = new ArrayList<IBShort>(ibsrecievedorder);
			b.ibsrejectedbank = new ArrayList<Integer>(ibsrejectedbank);
			b.ibsconnectedbank = new ArrayList<Integer>(ibsconnectedbank);
			b.ibsnotconnectedbank = new ArrayList<Integer>(ibsnotconnectedbank);
			b.Slist = new ArrayList<Double>(Slist);
			b.goallevt = new ArrayList<Double>(goallevt);
			b.sCARlist=new ArrayList<Double>(sCARlist);

			return b;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}


////////////Reinforce Learning ///////////////////////////////////////////////////////////////////////////////////////

	public void FirstDecideLev() {
		/*
		BigDecimal c=new BigDecimal(String.valueOf(CAR));
		int car=(int)c.setScale(4,BigDecimal.ROUND_HALF_UP).doubleValue();
		*/

		/*
				nextlev=CAR;
				levlist.stream().filter(s->s.level==nextlev)
								.forEach(s->s.state="Yes");
				levlevellist.add(nextlev);
				*/
	}

	public void calcEValue(double h) {
		/*
				levlist.stream().forEach(s->{
					s.evalue-=h*s.evalue;
					s.setStrength();
			});
			*/
	}

	public void setAdjacent() {
		/*
				for(int i=0;i<levlist.size();i++){
					if(levlist.get(i).state=="Yes"){
						levlist.get(i).adjacent="Yes";
						if(i>0)levlist.get(i-1).adjacent="Yes";
						if(i<levlist.size()-1)levlist.get(i+1).adjacent="Yes";

					}
				}

		*/
	}

	public void setAvaRange() {
		/*
				levlist.stream().filter(s->s.state=="Yes")
				.forEach(s->levnownum=s.levnum);

				int i;
				i=levnownum-3;if(i<0)i=0;

				for(int j=i;j<=levnownum+3;j++) {
				if(j>=levlist.size()) {break;}else {
				levlist.get(j).range="Yes";}
				if(levlist.get(j).adjacent=="Yes")levlist.get(j).finalrange="Yes";

				}

				int count=0;
				int maxrangenum=0;
				for(int j=0;i<levlist.size();i++){
				if(levlist.get(i).range=="Yes") maxrangenum=levlist.get(i).levnum;
				if(levlist.get(i).finalrange=="Yes") count++;
				}

				if(count==0){
				nextlev=levlist.get(maxrangenum).levnum;
				levlist.get(maxrangenum).state="Yes";
				}
				*/
	}

	public void choiceLev() {
		/*
				levlist.stream().filter(s->s.finalrange=="Yes")
		.forEach(s->sigmat+=Math.exp(s.X));


				levlist.stream().filter(s->s.finalrange=="Yes")
		.forEach(s->{s.pl=Math.exp(s.X)/sigmat;
		counter+=s.pl;
		LevProbably l=new LevProbably(s.levnum,counter,s.level);
		list.add(l);
		});

				double r=Math.random();//乱数生成
				double a=0;
				for(LevProbably l:list) {
				if(r<=l.getCount()&&a<=r) {
				nextlev=l.getLevel();
				break;
				}else {
				a=l.getCount();
				}
				}

				goallevt.add(nextlev);

				levlist.stream().filter(s->s.level==nextlev)
							.forEach(s->s.state="Yes");
				levlist.stream().filter(s->s.level!=nextlev)
						.forEach(s->s.state="No");
				for(Leverage l:levlist) {
				if(l.level-0.005==nextlev||l.level+0.005==nextlev) {l.adjacent="Yes";}else {l.adjacent="No";}
				}

				levlevellist.add(nextlev);
		*/
	}

	public void EvaLeverage(double sm) {
		/*
				levlist.stream().filter(s->s.state=="Yes")
					.forEach(s->s.evalue=(1-sm)*s.evalue+sm*pai);

				levlist.stream().filter(s->s.evalue<0)
					    		.forEach(s->s.evalue=0);
					    		*/

	}

	public void setLev(double lbmin, double lbmax, double val, String state, String range, double vi) {
		/*
				int size=(int) (((lbmax-lbmin))/val)+1;
				double value=lbmin;
				BigDecimal b1=new BigDecimal(value);
				BigDecimal b2=new BigDecimal(val);

				for(int i=0;i<size;i++) {
					Leverage newlev=new Leverage(value,0,0,state,range,i,vi);
					levlist.add(newlev);

					BigDecimal result=b1.add(b2);
					b1=result;
					value=result.doubleValue();
				}

		*/

	}

	public void setActualLeverage() {

		actlevt.add(CAR);
	}




}
