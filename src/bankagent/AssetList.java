package bankagent;

public class AssetList implements Cloneable{

	public int id, group;
	public double vol, amount, flucrate, curvalue, ascor, def;
	public double sellamount, buyamount;//売り，買いが行った時に代入
	public String state;//buy,sell,pose

	public AssetList(int id, int group, double vol, double amount, double ascor) {
		this.id = id;
		this.group = group;
		this.vol = vol;
		this.amount = amount;
		this.ascor = ascor;
		curvalue = amount;
	}

	//after longterm to shortterm
	public void addAmount(double addamount) {
		amount += addamount;
		curvalue += addamount;
	}

	public void cutAmount(double cutamount) {
		amount -= cutamount;
		curvalue -= cutamount;
	}

	public void setFlucrate(double rate) {
		flucrate += rate;
	}

	public void setCuramount() {
		curvalue = amount * (1 + flucrate);
		def = curvalue - amount;//Asset現在の損益
		state = "pose";
	}

	public void setPartOfSellAmount(double sell) {
		curvalue -= sell;
		amount -= sell;
		sellamount = sell;
		state = "sell";
	}

	public void setFullSellAmount() {
		sellamount = amount;
		curvalue = 0;
		amount = 0;
		state = "sell";
	}

	public AssetList clone() {
		try {
			AssetList asl=(AssetList) super.clone();
			return asl;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}


}
