package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import NetworkBA_TD.Corres;
import NetworkBA_TD.Network;
import NetworkBA_TD.Virtual_BA;
import asset.Asset;
import bankagent.AssetList;
import bankagent.IBShort;
import bankagent.Institution;
import comparator.Bank_Sumdeg_BA_Comp;
import networkCP.NetworkCP1;

public class BuildNWByThreshold {

	public ArrayList<Institution> bank = new ArrayList<Institution>();
	public ArrayList<Institution> clbank = new ArrayList<Institution>();
	public ArrayList<Asset> asset = new ArrayList<Asset>();
	public boolean isCreated;//

	public double theta;//TD 閾値
	public int m0,m;//BA
	public int degsum;//総次数
	public double wdegsum=0;
	public double ibstock;
	public double totalmoney;
	public double bscallrate;

	//cp variable
	public int degba;
	public double randIn,randOut;
	public double a1,a2;

	//create network model of object
	ArrayList<Virtual_BA> v_ba = new ArrayList<Virtual_BA>();


	public void setInfo() {
		bank.stream().forEach(s -> ibstock += s.bb + s.lb);
	}

//// TD Network ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public BuildNWByThreshold(ArrayList<Institution> bank, ArrayList<Asset> asset, double theta, double bscallrate, ArrayList<Institution> clbank) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
		this.asset.addAll(asset);
		this.theta = theta;
		this.bscallrate = bscallrate;
		this.clbank.addAll(clbank);
		setInfo();
	}

////CP Network ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	public BuildNWByThreshold(ArrayList<Institution> Bank, ArrayList<Asset> asset, double bscallrate, double randIn,
			double randOut, int degba, double a1, double a2, ArrayList<Institution> clbank) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(Bank);
		this.asset.addAll(asset);
		this.bscallrate = bscallrate;
		this.randIn = randIn;
		this.randOut = randOut;
		this.degba = degba;
		this.a1 = a1;
		this.a2 = a2;
		this.clbank.addAll(clbank);
		setInfo();
	}

////BA Network ///////////////////////////////////////////////////////////////////////////////////////////////////////////


	public BuildNWByThreshold(ArrayList<Institution> Bank, ArrayList<Asset> asset, int m0, int m, double bscallrate,
			ArrayList<Institution> clbank) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(Bank);
		this.asset.addAll(asset);
		this.m0 = m0;
		this.m = m;
		this.bscallrate = bscallrate;
		this.clbank.addAll(clbank);
		setInfo();
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void isStart(String nw) {
		// I/Bの部分を消去
		deleteIBTranxList();

		//arraylistクラスのクローン作成
		createObject_ObjectClone();
		AdjustAsset();

		bank.stream().forEach(s -> s.setInitIBBS());

		// 新たにI/Bネットワークの作成
		createNW(nw);

		//calc weightdegsum
		IBdeg();

		//call loan,moneyの決定
		IBtranx();

		//adjust BS
		AdjustBS();

		//setCARD
		bank.stream().forEach(s->s.CARD = -10000);
	}

	public void createObject_ObjectClone() {
		//clone先からのassetlistのコピー
		/*
		 * i/bの部分は新規作成するため，assetの部分を構築(同じアセットを持たせるため)
		 */
		for(Institution b:bank) {
			b.Exasset.clear();
			for(Institution clb:clbank) {
				if(b.id == clb.id) {
					for(AssetList aslist:clb.Exasset) {
						b.Exasset.add(aslist.clone());
					}
				}
			}
		}
	}

	public void AdjustAsset() {

		for(Institution b:bank) {
			for(AssetList aslist:b.Exasset) {
				for(Asset as:asset) {
					if(as.id == aslist.id) as.setTotalAmount(aslist.amount);
				}
			}
		}
	}


	public void AdjustBS() {

		bank.stream().forEach(s -> s.setInitIBBS());

		for (Institution b : bank) {
			if (b.totalassets > b.totalinvest) {
				//liqAを均等に追加
				double dif = b.totalassets - b.totalinvest;
				b.addExAsset(asset, dif);
			}
			if (b.totalassets < b.totalinvest) {
				//liqAを均等に減少
				double dif = b.totalinvest - b.totalassets;
				b.cutExasset(asset, dif);
			}
		}

		bank.stream().forEach(s -> {
			s.setInitIBBS();
			//nの帳尻を合わせる
			s.adjustNetWorth();
		});

	}

	public void IBtranx() {
		for (Institution b : bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				IBShort ibs = new IBShort(b.Inid.get(i), ibstock * b.cpbrin.get(i) / wdegsum);
				b.ibsborrow.add(ibs);
				b.ibsconnectedbank.add(b.Inid.get(i));
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				IBShort ibs = new IBShort(b.Outid.get(i), ibstock * b.cpbrout.get(i) / wdegsum);
				b.ibsloan.add(ibs);
				if (!b.ibsconnectedbank.contains(b.Outid.get(i)))
					b.ibsconnectedbank.add(b.Outid.get(i));
			}
		}
		//ibsの決定
		for (Institution b : bank) {
			for (IBShort ib : b.ibsloan) {
				b.lb += ib.money;
			}
			for (IBShort ib : b.ibsborrow) {
				b.bb += ib.money;
			}
		}

	}


	public void IBdeg() {
		//IB取引額の重み
		for (Institution b : bank) {
			for (int i = 0; i < b.Inid.size(); i++) {
				for (Institution bb : bank) {
					if (b.Inid.get(i) == bb.id) {
						b.cpbrin.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
			for (int i = 0; i < b.Outid.size(); i++) {
				for (Institution bb : bank) {
					if (b.Outid.get(i) == bb.id) {
						b.cpbrout.add(b.deg + bb.deg);
						wdegsum += b.deg + bb.deg;
					}
				}
			}
		}
	}

	public void createNW(String nw) {

		switch(nw) {
		case "TD":
			Network TD = new Network(bank.size(), theta);
			TD.createBA();
			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = TD.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = TD.getOutid(i);//出て行ってい金融機関のid
			}
			//In,Outid=0をdelete
			for (Institution b : bank) {
				Iterator<Integer> inid = b.Inid.iterator();
				while (inid.hasNext()) {
					Integer id = inid.next();
					if (id == 0)
						inid.remove();
				}
				Iterator<Integer> outid = b.Outid.iterator();
				while (outid.hasNext()) {
					Integer id = outid.next();
					if (id == 0)
						outid.remove();
				}
			}
			break;
		case "CP":
			NetworkCP1 CP = new NetworkCP1(bank.size(), a1, a2, randIn, randOut);
			isCreated = CP.createCP_sameDegBA(degba);
			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = CP.getInid(i);// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = CP.getOutid(i);//出て行ってい金融機関のid
			}
			CP.virtualClear();
			break;
		case "BA":

			Network BA = new Network(bank.size());
			int h = 0;
			do{
				BA.clear();
				h=0;
				BA.createKE2(m0, m, 5, 0);
				//if(BA.getNum_of_sumdeg());

				for(int i=0;i<bank.size();i++){
					if(BA.getOutdegree(i)==0 || BA.getIndegree(i)==0){
						h++;
					}
				}
			}while(h!=0);

			for(int i=0 ; i< bank.size() ; i++) {
				Virtual_BA v = new Virtual_BA(i+1,BA.getInid(i),BA.getOutid(i));
				v_ba.add(v);
			}

			//v_ba listを総次数で降順に
			Collections.sort(v_ba, new Bank_Sumdeg_BA_Comp());

			//現在のbankとvbaのid対応リストを作る
			ArrayList<Corres> crs = new ArrayList<Corres>();
			for(int i = 0 ; i < bank.size() ; i++) {
				Corres cor = new Corres(bank.get(i).id , v_ba.get(i).id);
				crs.add(cor);
			}

			//v_ba のそれぞれのリストに対応リストを照合させ，newリストを作る
			for(Virtual_BA vba :v_ba) {
				for(Corres cor : crs) {
					if(vba.id == cor.oldid) vba.newid = cor.newid;
				}
			}
			for(Virtual_BA vba : v_ba) {
				for(Integer inid : vba.inid) {
					for(Corres cor :crs) {
						if(inid == cor.oldid) vba.new_inid.add(cor.newid);
					}
				}
			}

			for(Virtual_BA vba : v_ba) {
				for(Integer outid : vba.outid) {
					for(Corres cor :crs) {
						if(outid == cor.oldid) vba.new_outid.add(cor.newid);
					}
				}
			}

			for (int i = 0; i < bank.size(); i++) {
				bank.get(i).Inid = v_ba.get(i).get_new_Inid();// <arraylist>i番目の銀行の入ってきている銀行のidリスト
				bank.get(i).Outid = v_ba.get(i).get_new_Outid();//出て行ってい金融機関のid
			}

			for(Institution b:bank) {
				if(b.Inid.contains(b.id)) System.out.println("Same index Inid!!");
				if(b.Outid.contains(b.id)) System.out.println("Same index Outid!!");
			}

			//In,Outid=0をdelete
			for (Institution b : bank) {
				Iterator<Integer> inid = b.Inid.iterator();
				while (inid.hasNext()) {
					Integer id = inid.next();
					if (id == 0)
						inid.remove();
				}
				Iterator<Integer> outid = b.Outid.iterator();
				while (outid.hasNext()) {
					Integer id = outid.next();
					if (id == 0)
						outid.remove();
				}
			}
			v_ba.clear();
		}
		//総次数の計算
		bank.stream().forEach(s->s.deg = s.Inid.size()+s.Outid.size());
		for(Institution b:bank) degsum += b.deg;
	}

	public void deleteIBTranxList() {
		bank.stream().forEach(s -> {
			s.Inid.clear();
			s.Outid.clear();
			s.cpbrin.clear();
			s.cpbrout.clear();
			s.ibsborrow.clear();
			s.ibsloan.clear();
			s.ibsconnectedbank.clear();
			s.lb = 0;
			s.bb = 0;
			s.deg = 0;
			s.indeg = 0;
			s.outdeg = 0;
		});
	}

	public int getDegSum() {
		// TODO 自動生成されたメソッド・スタブ
		return degsum;
	}
}
