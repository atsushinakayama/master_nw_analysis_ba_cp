package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import asset.Asset;
import bankagent.DemandOrderList;
import bankagent.Institution;
import bankagent.LInterest;
import calculator.CalcDebtRank;
import calculator.CalcPageRank;
import comparator.GetFirmKComparator;
import firmagent.CandidateBank;
import firmagent.Enterprise;
import firmagent.OrderList;
import home.Correlation;
import output.MiddleOutPut;
import output.MiddleOutPutFirm;
import output.OutPut;
import output.OutPutShort;

public class Experiment {

	ArrayList<Institution> Bank = new ArrayList<Institution>();
	ArrayList<Institution> DeadBank = new ArrayList<Institution>();
	ArrayList<Institution> SBank = new ArrayList<Institution>();
	ArrayList<Enterprise> Firm = new ArrayList<Enterprise>();
	ArrayList<BrProcess> Vand = new ArrayList<BrProcess>();
	ArrayList<Asset> Asset = new ArrayList<Asset>();

	private int atpnum;
	private int banknode, firmnode;
	private double k0;
	private double r;//市場金利
	private double rr;//金利係数
	private double dr;//預金金利
	private double liqAr;//市場性資産収益率
	private double brCAR;//銀行破綻条件
	private double div;//配当
	private int tau;//返却期限
	private int term;//ターム数
	public int x;//タームインデックス
	private int endterm;
	private int creditterm;//クレジット市場のターム数
	private int cmiterator;
	private int startterm;
	private double z;//自己資本比率破綻条件
	private int fz;//企業市場からの撤退条件
	private double rz;//自己資本維持率
	private int d;
	private double e;
	private double h;//forgetting process
	private double sm;//strength memory
	private double fii, b;//企業限界生産型
	private double pi;
	private double f;
	private int maxmainbank;
	private int minmainbank;
	private String expstate;

	//shorttermvariable
	private int startshortterm;
	private int shortterm;
	private double depoutrate;
	private double vol;
	private double shockflucrate;

	//お試し
	//金利
	double gatti = 0.01;
	//固定費
	double kt = 0.02;

	//金融機関
	double xx, psy, alpha, myu, sita, bscallrate;//riskaversion係数、乗数、riskcoefficient係数、貸出金利係数、乗数
	//企業
	double fi, g, c, lamda, initialbanknumber, beta;//生産能力、変動費率、破産コスト係数、partnerchoice,初期取引銀行候補先,生産能力係数
	double stc;//固定費

	ArrayList<Double> Prod = new ArrayList<Double>();//生産量
	ArrayList<Double> ProdGrowthRate = new ArrayList<Double>();//生産量成長率
	ArrayList<Integer> NumberOfFailedFirm = new ArrayList<Integer>();//企業破綻数
	ArrayList<Enterprise> FailedFirm = new ArrayList<Enterprise>();
	ArrayList<Institution> FailedBank = new ArrayList<Institution>();

	ArrayList<Correlation> cor = new ArrayList<Correlation>();

	int newfirmid;
	int initfirmnode;
	int noobfirm;

	double lfmin, lfmax, val, vi;//企業レバレッジ
	int Candmax;//企業最大取引先

	int brfnum;//総破綻企業e

	//OutPutリスト(longterm)
	ArrayList<MiddleOutPut> mop = new ArrayList<MiddleOutPut>();
	ArrayList<MiddleOutPutFirm> mopf = new ArrayList<MiddleOutPutFirm>();

	int ffnum;//タームごと企業破綻数カウンタ
	ArrayList<Integer> FFnum = new ArrayList<Integer>();//タームごと企業破綻数
	ArrayList<Double> FFrate = new ArrayList<Double>();//タームごと企業破綻割合

	double grossproduction;//生産量カウンタ
	ArrayList<Double> GrossProduction = new ArrayList<Double>();//タームごと総生産

	double grossbadloan;
	ArrayList<Double> GrossBadLoan = new ArrayList<Double>();//タームごとbadloan

	double grossr;
	int rcounter;
	ArrayList<Double> AvgInterest = new ArrayList<Double>();

	double moneystock;
	ArrayList<Double> MoneyStock = new ArrayList<Double>();

	double creditmoney;
	ArrayList<Double> CreditMoney = new ArrayList<Double>();

	double residual;
	ArrayList<Double> Residual = new ArrayList<Double>();

	double grossdemand;
	ArrayList<Double> Grossdemand = new ArrayList<Double>();

	double grosssupply;
	ArrayList<Double> Grosssupply = new ArrayList<Double>();

	int rf = 0;//Rejectfinancing(融資拒否数)
	ArrayList<Integer> RejectFinancing = new ArrayList<Integer>();

	double grossfirmlev;
	ArrayList<Double> Avgfirmlev = new ArrayList<Double>();

	double grossbankclientnum;
	ArrayList<Double> Avgbankclientnum = new ArrayList<Double>();

	double grossbankprofit;
	ArrayList<Double> GrossBankProfit = new ArrayList<Double>();

	double grossbankbuffer;
	ArrayList<Double> GrossBankBuffer = new ArrayList<Double>();

	//OutPut(shortterm)
	int failedbanknum,failedbankcar,failedbankgap;
	ArrayList<Integer> Fbanknum=new ArrayList<Integer>();
	ArrayList<Integer> Alivebanknum=new ArrayList<Integer>();
	ArrayList<Integer> FbankBoth=new ArrayList<Integer>();
	ArrayList<Integer> FbankCAR=new ArrayList<Integer>();
	ArrayList<Integer> FbankGap=new ArrayList<Integer>();
	ArrayList<Integer> FbankCM=new ArrayList<Integer>();
	ArrayList<Integer> FbankMega=new ArrayList<Integer>();
	ArrayList<Integer> FbankMedium=new ArrayList<Integer>();
	ArrayList<Integer> FbankSmall=new ArrayList<Integer>();
	ArrayList<Double> NotRollOver=new ArrayList<Double>();
	ArrayList<Double> IBSupply=new ArrayList<Double>();
	ArrayList<Double> ClusterCoef = new ArrayList<Double>();
	ArrayList<Double> AvgVertexDistance = new ArrayList<Double>();
	ArrayList<Double> DifPageRank = new ArrayList<Double>();
	ArrayList<Double> MaxDifPageRank = new ArrayList<Double>();
	ArrayList<Double> MinDifPageRank = new ArrayList<Double>();
	ArrayList<Double> AvgDebtRank = new ArrayList<Double>();

	double IBmoneystock;
	ArrayList<Double> IBMoneyStock=new ArrayList<Double>();


	int brcount = 0;

	public Experiment(ArrayList<Institution> Bank, ArrayList<Enterprise> Firm, ArrayList<BrProcess> vand, int banknode,
			int firmnode, double r, int tau, int term, double z, double xx,
			double psy, double alpha, double myu, double sita, double fi, double g, double c, double lamda,
			int initialbanknumber, int d, double e, double k0, double dr, int startterm, double div, double liqAr,
			int creditterm, double bscallrate, double rz, double beta, double h, double sm, double lfmin, double lfmax,
			double val, double pi, double vi, int Candmax, double rr, int shortterm, double depoutflow,
			double vol, ArrayList<Asset> Asset, double brCAR, double shockflucrate, int cmiterator, int fz, double stc,
			double f, int maxmainbank, int startshortterm, ArrayList<MiddleOutPut> mop,
			ArrayList<MiddleOutPutFirm> mopf, int mimmainbank, ArrayList<Correlation> cor, int atpnum,String expstate) {
		// TODO Auto-generated constructor stub
		this.Bank.addAll(Bank);
		this.Firm.addAll(Firm);
		this.Vand.addAll(vand);
		this.banknode = banknode;
		this.firmnode = firmnode;
		this.r = r;
		this.tau = tau;
		this.term = term;
		this.z = z;
		this.xx = xx;
		this.psy = psy;
		this.alpha = alpha;
		this.myu = myu;
		this.sita = sita;
		this.fi = fi;
		this.g = g;
		this.c = c;
		this.lamda = lamda;
		this.initialbanknumber = initialbanknumber;
		this.d = d;
		this.e = e;
		this.k0 = k0;
		this.dr = dr;
		this.startterm = startterm;
		this.div = div;
		this.liqAr = liqAr;
		this.creditterm = creditterm;
		this.bscallrate = bscallrate;
		this.rz = rz;
		this.beta = beta;
		this.h = h;
		this.sm = sm;
		this.lfmin = lfmin;
		this.lfmax = lfmax;
		this.val = val;
		this.pi = pi;
		this.vi = vi;
		this.Candmax = Candmax;
		this.rr = rr;
		this.shortterm = shortterm;
		this.depoutrate = depoutflow;
		this.vol = vol;
		this.Asset.addAll(Asset);
		this.brCAR = brCAR;
		this.shockflucrate = shockflucrate;
		this.cmiterator = cmiterator;
		this.fz = fz;
		this.stc = stc;
		this.f = f;
		this.maxmainbank = maxmainbank;
		this.startshortterm = startshortterm;
		this.mop.addAll(mop);
		this.mopf.addAll(mopf);
		this.cor.addAll(cor);
		this.atpnum = atpnum;
		this.expstate=expstate;

		this.newfirmid = firmnode + 1;
		this.initfirmnode = firmnode;
	}

	public void isStart() {
		// TODO Auto-generated method stubdd

		for (int x = 0; x < term; x++) {
			if(expstate=="Build") {
			if (banknode == 0)
				break;
			if (FailedBank.size() > 0) {
				brcount++;
				break;
			}
			}


			//OutPut変数初期化
			InitOPVariables();
			//各エージェント変数初期化
			InitVariable(Bank, Firm);

			//返却金利等の計算（この時点で計算しておく）
			Debt(Firm, Bank, x);

			AdjustBS(Firm, Bank, x);
			//金融機関目標レバレッジへの調整
			setForceLeverage(Bank, x);
			AdjustBS(Firm, Bank, x);
			//企業最適資本の設定(期待利益の計算＋借入金の設定）
			//FirmOptimalStock(Firm,x);
			//企業今期レバレッジの選択
			setFirmForceLeverage(Firm, x);//自己組織型

			setSupplyAndDemand(Bank, Firm, x);

			for (int y = 0; y < creditterm; y++) {
				//1取引銀行貸出可能額の設定(流動性規制)
				BankSupplyLiq(Bank, x);

				//銀行貸出金利の設定（全ての企業に対して）
				BankDecideInterest(Bank, Firm, banknode, firmnode, r);

				//企業取引先の決定(partnerchoice) //??partnerchoiceで取引候補先の追加はするのか
				//FirmDecideBank(Firm,Bank,lamda,x);
				FirmDecideBank1(Firm, Bank, x);

				BankDecideFirm(Firm, Bank, x);
				//企業への貸付(Credit市場)
				CreditMarket(Firm, Bank, x);
				//企業Candidatelistの更新
				FirmSetCandidate(Firm, x);

				//secondCreditMarket(余剰金の貸し付け)
				SecondCreditMarket(Firm, Bank, x);

				AdjustBS(Firm, Bank, x);

			}




			//銀行credit市場後のbuffer格納リスト
			BankinputBuffer(Bank, x);

			//銀行企業CMリストの更新//企業realLeverageの格納
			UpdateCMList(Bank, Firm, x);

			for(Institution b:Bank) {
				double a=0;
				a = Math.abs(b.totalassets-b.totalinvest);
				if(a>0.01) {
					System.out.println("longterm");
					System.out.println(b.id+"銀行 car:"+b.CAR+" n:"+b.n+" buffer;"+b.buffer+" shortage"+b.shortage+" a"+a);
					System.out.println("差:"+(b.totalassets-b.totalinvest));
				}
			}


			if (expstate == "Start") {
				ShortTermStart(Bank, shortterm);
			}

			//企業生産高
			//FirmProductionProfit(Firm,x);
			//企業生産高（限界生産）
			FirmProductionProfit1(Firm, x);

			//企業割賦（分割払い＋金利）支払い
			FirmPayInstallments(Firm, Bank, Vand, x);



			//企業ターム始め利益(生産活動から）の決定
			//FirmRealProfit(Firm,x);
			FirmRealProfit1(Firm, x);

			//企業破綻処理
			BankruptFirm(Firm, x);

			//銀行金利（貸出金)の返却
			BankPayInstallments(Bank, Vand);
			//銀行利益の決定（+ショック）(CreditMarket)
			AdjustBS(Firm, Bank, x);

			BankProfit(Bank, Firm, dr, x, div);
			//銀行　buffer shortage 調整
			BankAdjustBuf();
			//実利益から選択したレバレッジの評価
			EvaLev(Firm, Bank, x);
			//バランスシート調整
			AdjustBS(Firm, Bank, x);
			//銀行破綻処理+連鎖破綻処理(I/BMarket)
			ChainOfBankruptBank(Bank, x, r);
			//銀行最終利益
			BankRealProfit(Bank, x, r);
			AdjustBS(Firm, Bank, x);
			//破綻行をリストから消去
			BankruptProcessing(Bank, Firm, x);

			//Buffer<0の処理
			if(expstate=="Build") {
			BankBufferProcessing(Bank, x);
			AdjustBS(Firm, Bank, x);
			}
			//OPリストに値を代入
			InputOPList(x);

			//各エージェントリストの更新,消去
			updateList(Bank, Firm, FailedBank, FailedFirm, x);

			//企業の参入
			NewEntry(Firm, Bank, x);
			//企業最大取引数
			FirmSetMainBankNum();
			Firm.stream().forEach(s -> s.live++);
		}

	}

	/*
	public void createBankBSComp(int la, int megala, int mediumla, int smallla, String nw, double theta, double randIn, double randOut, double a1, double a2) {
		BuildComplete BankBSCom = new BuildComplete(Bank, Asset, bscallrate, nw,theta ,randIn,randOut,a1,a2);
		BankBSCom.isStart();
		BankBSCom.allocExasset(la, megala, mediumla, smallla);
		Bank.clear();
		Bank.addAll(BankBSCom.getFinalBank());
	}
	*/

	public void createMiddleOutPut() {
		for (int i = 0; i < mop.size(); i++) {
			mop.get(i).setResultAndCalc(Bank.get(i));
		}
		setSortFirmK();
		for (int i = 0; i < mopf.size(); i++) {
			mopf.get(i).setResultAndCalc(Firm.get(i));
		}
		//相関の計算
		for (Correlation c : cor) {
			if (c.i == atpnum) {
				c.calcCorrelation(Firm);
			}
		}
	}

	public void ShorttermOutputInit() {
		Bank.stream().forEach(s->{
			IBmoneystock+=s.bb;
			IBmoneystock+=s.lb;
		});
		IBMoneyStock.add(IBmoneystock);

	}
	public void getOutPutVariablesIBS(IBshortMarket ibshort) {
		NotRollOver.add(ibshort.getNotRollOverAmount());
		IBSupply.add(ibshort.getIBSupply());
	}

	public void getOutPutVariablesBR(BankruptProcessing br) {
		Alivebanknum.add(Bank.size());
		Fbanknum.add(br.getFbanknum());
		FbankBoth.add(br.getFbankBoth());
		FbankCAR.add(br.getFbankCAR());
		FbankGap.add(br.getFbankGap());
		FbankMega.add(br.getFbankMega());
		FbankMedium.add(br.getFbankMedium());
		FbankSmall.add(br.getFbankSmall());
		//SBank.addAll(br.getDeadBank());
	}


	public void CalcNWCoef() {

		CalcPageRank calcpr = new CalcPageRank(Bank);
		//PageRank
		calcpr.calcDifPageRank();
		DifPageRank.add(calcpr.getDifPageRank());
		MaxDifPageRank.add(calcpr.getMaxDifPageRank());
		MinDifPageRank.add(calcpr.getMinDifPageRank());
		calcpr.clearList();

		//DebtRank
		CalcDebtRank calcdr = new CalcDebtRank(Bank);
		calcdr.isStart1();
		Bank.stream().forEach(s -> s.setDebtRank(calcdr.getDebtRank(s.id)));
		calcdr.clealList();

		double sumdr = 0;
		for(Institution b:Bank) {
			sumdr += b.dr/Bank.size();
		}
		AvgDebtRank.add(sumdr);
		sumdr = 0;

	}

	public void ShortTermStart(ArrayList<Institution> bank, int shortterm) {
		/*
		 * depoutrateはランダムにしてもよいかも
		 * ここではexasset,bufferの調整のみ
		 * BS全体の調整は行わない(dの調達)
		 *
		 */



		//shortterm output init
		//ShorttermOutputInit();

		//System.out.println("-----------shortterm start--------------");
		for (int i = 1; i <= shortterm; i++) {
			//System.out.println("shorterm "+i+"term");

			//bankinitialize(ibs=0,connectの処理)
			bank.stream().forEach(s -> s.IBSInit(bank.size()));
			bank.stream().forEach(s -> s.setIBShortBS());


			for(Institution b:Bank) {
				double a=0;
				a = Math.abs(b.totalassets-b.totalinvest);
				if(a>0.01) {
					System.out.println(i+"term begin");
					System.out.println("failedbanksize:"+FailedBank.size());
					System.out.println(b.id+"銀行 car:"+b.CAR+" n:"+b.n+" buffer;"+b.buffer+" shortage"+b.shortage+" a"+a);
				}
			}


			//Assetinitialize
			Asset.stream().forEach(s -> {
				//Asset>ボラティリティ変動幅
				s.setVolFlucrate();
			});

			//bank deposit out and sellorbuy asset
			bank.stream().forEach(s -> {
				//Deposit変動
				s.depOut(depoutrate);
				//AssetList変動評価損益計算(Asset参照)
				s.asFluctuation(Asset);
				s.setIBShortBS();
				//sellAsset
				//s.sellAsset();
				//buyAsset
				//s.buyAsset();
				//s.setIBShortBS();
			});

			//set selled,bought amount
			//need Asset flucrate initialize
			/*
			for (Institution b : bank) {
				for (AssetList aslist : b.Exasset) {
					for (Asset as : Asset) {
						if (as.id == aslist.id) {
							switch (aslist.state) {
							case "buy":
								as.setBuyAmount(aslist.buyamount);
								break;
							case "sell":
								as.setSellAmount(aslist.sellamount);
								break;
							case "pose":
								break;
							}
						}
					}
				}
			}
			*/
			/*
			Asset.stream().forEach(s -> {
				//set flucrate&totalamount by selled,bought amount
				//s.setBuyAndSellFluc(shockflucrate);
				//groupflucrate.correlation
				//s.setByGroupFluc(Asset);
			});
			*/

			//最終保持asset変動幅
			//bank.stream().forEach(s -> s.asFluctuation(Asset));
			//bank.stream().forEach(s -> s.setIBShortBS());

			//IBmarket
			IBshortMarket ibshort = new IBshortMarket(Bank);
			ibshort.isStart();
			bank.clear();
			bank.addAll(ibshort.getBank());
			getOutPutVariablesIBS(ibshort);
			ibshort.AgentAndVariablesClear();

			bank.stream().forEach(s->s.setIBShortBS());

			//end term processing

			//asset
			Asset.stream().forEach(s -> {
				//時系列データの格納
				s.setTimeseriesData();
				//累積値の計算
				s.setAccumurateData();
				//asset initialize variables
				s.Init();
			});

			//reserve NW Value
			bank.stream().forEach(s -> s.IBSInit(bank.size()));
			bank.stream().forEach(s -> s.setIBShortBS());
			CalcNWCoef();

			//BankruptProcessing& set propagate(IB,Asset) & set firm bblist
			if(i!=shortterm) {
			BankruptProcessing br = new BankruptProcessing(Bank, Asset, shockflucrate, Firm);
			br.isStart(brCAR, grossbadloan);
			bank.clear();
			bank.addAll(br.getBank());
			FailedBank.addAll(br.getDeadBank());
			DeadBank.addAll(br.getDeadBank());

			getOutPutVariablesBR(br);
			br.AgentAndVariablesClear();

			bank.stream().forEach(s -> s.IBSInit(bank.size()));
			bank.stream().forEach(s -> s.setIBShortBS());


			for(Institution b:Bank) {
				double a=0;
				a = Math.abs(b.totalassets-b.totalinvest);
				if(a>0.01) {
					System.out.println(i+"term end");
					System.out.println(b.id+"銀行 car:"+b.CAR+" n:"+b.n+" buffer;"+b.buffer+" shortage"+b.shortage+" a"+a);
				}
			}
			}

		}

		//bankinitialize(ibs=0,connectの処理)
		bank.stream().forEach(s -> s.IBSInit(bank.size()));


	}

	public void OutPutShortResult(ArrayList<OutPutShort> outputshort,int atp) {
		outputshort.get(atp).FailedBankNum.addAll(Fbanknum);
		outputshort.get(atp).FailedBankBoth.addAll(FbankBoth);
		outputshort.get(atp).FailedBankCAR.addAll(FbankCAR);
		outputshort.get(atp).FailedBankGap.addAll(FbankGap);
		outputshort.get(atp).FailedBankCM.addAll(FbankCM);
		outputshort.get(atp).FailedBankMega.addAll(FbankMega);
		outputshort.get(atp).FailedBankMedium.addAll(FbankMedium);
		outputshort.get(atp).FailedBankSmall.addAll(FbankSmall);
		outputshort.get(atp).NotRollOver.addAll(NotRollOver);
		outputshort.get(atp).IBSupply.addAll(IBSupply);
		outputshort.get(atp).AliveBankNum.addAll(Alivebanknum);
		outputshort.get(atp).DifPageRank.addAll(DifPageRank);
		outputshort.get(atp).MaxDifPageRank.addAll(MaxDifPageRank);
		outputshort.get(atp).MinDifPageRank.addAll(MinDifPageRank);
		outputshort.get(atp).AvgDebtRank.addAll(AvgDebtRank);
		outputshort.get(atp).FailedBank.addAll(DeadBank);
		
		outputshort.get(atp).setFailed_Bank_by_size(DeadBank);
	}

	public int getBrCount() {
		// TODO 自動生成されたメソッド・スタブ
		return brcount;
	}

	private void setSupplyAndDemand(ArrayList<Institution> bank, ArrayList<Enterprise> firm, int x) {
		// TODO Auto-generated method stub

		/*
		for(int i=0;i<banknode;i++){
			grosssupply+=bank.get(i).buffer;
		}
		for(int i=0;i<firmnode;i++){
			grossdemand+=firm.get(i).L;
		}
		*/
		bank.stream().forEach(s -> grosssupply += s.buffer);
		firm.stream().forEach(s -> grossdemand += s.L);

		Grosssupply.add(grosssupply);
		Grossdemand.add(grossdemand);

	}

	private void updateList(ArrayList<Institution> bank, ArrayList<Enterprise> firm, ArrayList<Institution> failedbank,
			ArrayList<Enterprise> failedfirm, int x) {
		// TODO 自動生成されたメソッド・スタブ

		//パラメタリストの更新
		for (int i = 0; i < firm.size(); i++) {
			if (x == 0) {
				firm.get(i).Alist.add(firm.get(i).A);
			} else {
				firm.get(i).updatePList();
			}
		}

		//firmレバレッジリストの更新
		firm.stream().forEach(s -> s.updateLevlist());

		//bankレバレッジリストの更新
		//bank.stream().forEach(s -> s.updateLevlist());

		for (int i = 0; i < bank.size(); i++) {
			if (x != 0) {
				bank.get(i).updateList();
			} else {
				//bank.get(i).CARlist.add(bank.get(i).CAR);
			}
		}

		//borrowingの相手の企業の状態の更新（破綻して入ればVandへ返済するため)
		for (int i = 0; i < firm.size(); i++) {
			for (int j = 0; j < firm.get(i).BB.size(); j++) {
				for (int k = 0; k < failedbank.size(); k++) {
					if (firm.get(i).BB.get(j).bankid == failedbank.get(k).id) {
						firm.get(i).BB.get(j).state = "Dead";
					}
				}
			}
			for (int j = 0; j < firm.get(i).newBB.size(); j++) {
				for (int k = 0; k < failedbank.size(); k++) {
					if (firm.get(i).newBB.get(j).bankid == failedbank.get(k).id) {
						firm.get(i).newBB.get(j).state = "Dead";
					}
				}
			}
		}



	}

	private void InitVariable(ArrayList<Institution> bank, ArrayList<Enterprise> firm) {
		bank.stream().forEach(s -> s.Init());
		firm.stream().forEach(s -> s.Init());
	}

	private void Debt(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {

		//金利支払い分の計算
		Firm.stream().forEach(s -> s.setInterestPayment());

	}

	private void setForceLeverage(ArrayList<Institution> bank, int x) {
		// TODO 自動生成されたメソッド・スタブ

		//目標レバレッジを初期自己資本比率に設定
		bank.stream().filter(s -> x == 0)
				.forEach(s -> s.setInitForceLev());

	}

	private void FirmOptimalStock(ArrayList<Enterprise> firm, int x) {
		// TODO Auto-generated method stub

		//期待利益の計算(一応）
		for (int i = 0; i < firmnode; i++) {
			firm.get(i).setBS(fi, beta);
			firm.get(i).setExpectedProfit(g);
		}

		//最適資本K*の計算　四捨五入
		for (int i = 0; i < firmnode; i++) {
			firm.get(i).setKK(g, c, fi, x);
		}

		//借入金の決定（K*とK(it)）
		for (int i = 0; i < firmnode; i++) {
			if (firm.get(i).KK - firm.get(i).K > 1) {
				firm.get(i).L = firm.get(i).KK - firm.get(i).K;
			} else {
				firm.get(i).L = 0;//最適資本が現在の資本より小さければ借入を行わない(L=0)に設定，後の条件分岐で
			}
		}

	}

	private void setFirmForceLeverage(ArrayList<Enterprise> firm, int x) {

		if (x == 0)
			firm.stream().forEach(s -> {
				s.DecideFirstLev();
			});

		//評価値,強度の計算
		firm.stream().forEach(s -> {
			s.calcEValue(h);
		});

		firm.stream().forEach(s -> s.setAdjacent());//選択可能レバレッジ範囲

		firm.stream().forEach(s -> s.setAvaRange());//近隣のレバレッジの選択

		//その中からレバレッジを選択する
		firm.stream().forEach(s -> s.choiceLev());

		//借入金の計算
		firm.stream().forEach(s -> s.setDemand());

	}

	private void BankSupplyLiq(ArrayList<Institution> bank, int x) {
		// TODO 自動生成されたメソッド・スタブ

		//銀行貸出可能額S(it)の設定
		for (Institution b : bank) {
			b.S = b.buffer;
			if (b.S < 0.01)
				b.S = 0;
			b.Slist.add(b.S);
		}

	}

	private void BankDecideInterest(ArrayList<Institution> bank, ArrayList<Enterprise> firm, int banknode, int firmnode,
			double r) {
		// TODO Auto-generated method stub

		//金利は少数第四位まで四捨五入

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < firm.size(); j++) {
				bank.get(i).addInterest(firm.get(j).id, r + firm.get(j).lev * rr);
			}
		}

	}

	private void FirmSetMainBankNum() {
		/*
		 * timestepごとにmainbank数が決まる
		 */
		/*
		int count = 0;
		for (Enterprise f : Firm) {
			count += f.K;
		}
		*/
		double maxK = 0;
		for (Enterprise f : Firm) {
			double count;
			count = f.K;
			if (maxK < count) {
				maxK = count;
			}
			count = 0;
		}
		for (Enterprise f : Firm) {
			f.setRelativeValue(maxK);
		}
		Firm.stream().forEach(s -> s.setMaxMainBankNum(Bank.size()));

	}

	private void FirmDecideBank1(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {

		//接続先の金利
		for (int i = 0; i < firm.size(); i++) {
			for (int j = 0; j < firm.get(i).Candidatelist.size(); j++) {//firmの銀行接続先
				for (int k = 0; k < bank.size(); k++) {
					if (firm.get(i).Candidatelist.get(j).id == bank.get(k).id) {

						for (int l = 0; l < bank.get(k).rf.size(); l++) {//bankのrf(貸出金利)list
							if (firm.get(i).id == bank.get(k).rf.get(l).id) {
								firm.get(i).Candidatelist.get(j).interest = bank.get(k).rf.get(l).r;
							}
						}
					}
				}
			}
		}

		//企業借入元本の作成
		for (Enterprise f : firm) {
			for (CandidateBank cb : f.Candidatelist)
				f.lp += cb.price;
		}

		//オーダーの作成(接続ネットワーク先に分割オーダーの作成)
		//借入金(返済前の借入金：price)に応じて要求金の作成
		//企業候補先がない場合はランダムにオーダーを作成
		for (Enterprise f : firm) {
			if (f.L > 0.001) {
				if (f.Candidatelist.size() != 0) {
					for (CandidateBank cb : f.Candidatelist) {
						//f.addOrderList(cb.id, (cb.grossl/f.l)*f.L, cb.interest, tau);
						if (f.lp != 0) {
							f.addOrderList(cb.id, (cb.price / f.lp) * f.L, cb.interest, tau);
						} else {
							f.addOrderList(cb.id, f.L, cb.interest, x);
						}
						f.NGlist.add(cb.id);//交渉を行った銀行idリスト
					}
				} else {
					int newbankid = (int) (banknode * Math.random()) + 1;
					double newr = 0;
					for (Institution b : bank) {
						for (LInterest r : b.rf) {
							if (f.id == r.id)
								newr = r.r;
						}
					}
					f.addCandidate(newbankid, newr, "Old", "No");
					f.addOrderList(newbankid, f.L, newr, x);
					f.NGlist.add(newbankid);
				}
			}
		}

	}

	private void BankDecideFirm(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {

		/*TODO
		 * Candidatelist 取引を行ったかいないかを更新すればok
		 *
		 */
		//銀行demandlistの格納
		for (Institution b : bank) {
			if (b.S > 1) {
				for (Enterprise f : firm) {
					for (OrderList ol : f.Order) {
						if (b.id == ol.bankid) {
							b.addDOL(f.id, ol.interest, ol.ordermoney, 1 - 0.001 * f.lev, tau);
							//b.addDOL(f.id,ol.interest,f.L,1-xx*Math.pow((f.lev/b.buffer),psy),tau);
						}
					}
				}
			}
		}

		//銀行demandlistからどの企業に貸し出すかを決定
		for (Institution b : bank) {
			Iterator<DemandOrderList> itDOL = b.DOL.iterator();
			while (itDOL.hasNext()) {
				double pro = Math.random();
				DemandOrderList dol = itDOL.next();
				if (dol.ps < pro) {
					itDOL.remove();
					//企業オーダー拒否カウンタ
					firm.stream().filter(s -> dol.firmid == s.id)
							.forEach(s -> s.rejectordernum++);
					rf++;
				} else {
					if (b.S > 0.001) {
						if (dol.l - b.S > 0.001) {
							dol.l = b.S;
							b.lft += b.S;
							b.S = 0;
							b.buffer = 0;//便宜上ここで0としておく

						} else {
							b.S -= dol.l;
							b.buffer -= dol.l;
							b.lft += dol.l;

						}
					} else {
						itDOL.remove();
					}
				}
			}
		}
		bank.stream().forEach(s -> s.lftlist.add(s.lft));

	}

	private void CreditMarket(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {
		// TODO Auto-generated method stub

		//銀行は企業からの要望リストDOLに貸付
		//各銀行のlftは0になるはずである．

		for (Institution b : bank) {
			Collections.shuffle(b.DOL);
			for (DemandOrderList dol : b.DOL) {
				for (Enterprise f : firm) {
					if (dol.firmid == f.id) {
						for (OrderList ol : f.Order) {
							if (ol.bankid == b.id) {
								if (b.lft > 0.01) {
									if (b.lft - ol.ordermoney > 0.01) {

										b.addLF(f.id, ol.ordermoney, dol.interest, tau);
										f.addBB(b.id, ol.ordermoney, dol.interest, tau);
										b.lft -= ol.ordermoney;

										//企業Candidatelistの更新
										for (CandidateBank cb : f.Candidatelist) {
											if (b.id == cb.id) {
												cb.tradestate = "Yes";
												cb.addPrincipal(ol.ordermoney);
											}
										}
										creditmoney += ol.ordermoney;
										f.L -= ol.ordermoney;
										grossr += dol.interest;
										rcounter++;
									} else {

										b.addLF(f.id, b.lft, dol.interest, tau);
										f.addBB(b.id, b.lft, dol.interest, tau);

										for (CandidateBank cb : f.Candidatelist) {
											if (b.id == cb.id) {
												cb.tradestate = "Yes";
												cb.addPrincipal(b.lft);
											}
										}

										creditmoney += b.lft;
										f.L -= b.lft;
										grossr += dol.interest;
										rcounter++;
										b.lft = 0;

									}
								}
							}
						}
					}
				}

			}
		}
		//企業buffer<0より融資を受けれなかった場合
		firm.stream().filter(s -> s.L > 0)
				.forEach(s -> s.nonbuffernum++);

	}

	private void FirmSetCandidate(ArrayList<Enterprise> firm, int x) {
		// TODO 自動生成されたメソッド・スタブ

		for (int i = 0; i < firmnode; i++) {
			for (int j = 0; j < firm.get(i).Candidatelist.size(); j++) {
				if (firm.get(i).Candidatelist.get(j).state == "New") {
					firm.get(i).Candidatelist.remove(j);
					firm.get(i).Candidatelistid.remove(j);
					j--;
				}
			}
		}

	}

	private void SecondCreditMarket(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {
		/*TODO
		 * まず企業側がdemandの残りを他銀行にオーダー
		 * 銀行側が利益獲得のためランダムで企業に接触
		 * buferがのこっているかつ供給可能流動性が残っている場合にsecondcredit市場へ参入
		 * 供給可能流動性がなくなるまでor企業の資金需要がなくなるまで貸出を行う
		 * 企業BBリストstateに新たな状態を追加したい
		 *
		 * regionに分けたいが、、、
		 */

		ArrayList<Institution> pbank = new ArrayList<Institution>();
		ArrayList<Enterprise> pfirm = new ArrayList<Enterprise>();

		bank.stream().filter(s -> s.buffer > 1)
				.forEach(s -> pbank.add(s));
		firm.stream().filter(s -> s.L > 0.5 && s.Candidatelist.size() < s.maxmainbank)
				.forEach(s -> pfirm.add(s));

		//企業-銀行ランダムマッチング
		int iterator = 1;

		if (pfirm.size() > 0 && pbank.size() > 0) {
			//どちらかのリストサイズが0になるかorイテレータ分
			//System.out.println("SecondCM:"+" firm;"+pfirm.size()+" pbank:"+pbank.size());
			do {

				//mainbank<candidatelistの場合、pfirmから消去
				Iterator<Enterprise> f = pfirm.iterator();
				while (f.hasNext()) {
					Enterprise ff = f.next();
					if (ff.Candidatelist.size() >= ff.maxmainbank) {
						f.remove();
					}
				}
				if (pfirm.size() == 0 || pbank.size() == 0)
					break;

				Collections.shuffle(pfirm);
				Collections.shuffle(pbank);
				//0番目同士を取引
				//貸し出すかの判定
				if (!pfirm.get(0).NGlist.contains(pbank.get(0).id)) {
					double rand = Math.random();
					if ((1 - 0.001 * pfirm.get(0).lev) > rand) {
						if (pfirm.get(0).L < pbank.get(0).buffer) {
							//bufferのほうが大きい場合
							/*
							pbank.get(0).addnewLFlist(pfirm.get(0).id, pfirm.get(0).L, rr * pfirm.get(0).lev + r, tau);
							pfirm.get(0).addnewBB(pbank.get(0).id, pfirm.get(0).L, rr * pfirm.get(0).lev + r, tau);
							*/
							pbank.get(0).addLF(pfirm.get(0).id, pfirm.get(0).L, rr * pfirm.get(0).lev + r, tau);
							pfirm.get(0).addBB(pbank.get(0).id, pfirm.get(0).L, rr * pfirm.get(0).lev + r, tau);
							pfirm.get(0).addCandidate(pbank.get(0).id, 0.001 * pfirm.get(0).lev + r, "Old", "Yes");

							for (CandidateBank cb : pfirm.get(0).Candidatelist) {
								if (cb.id == pbank.get(0).id) {
									cb.addPrincipal(pfirm.get(0).L);

									if (cb.tradestate == "No")
										cb.tradestate = "Yes";
								}

							}

							pbank.get(0).buffer -= pfirm.get(0).L;
							creditmoney += pfirm.get(0).L;
							pfirm.get(0).L = 0;
							grossr += pfirm.get(0).lev * 0.001 + r;
							rcounter++;
							pfirm.remove(0);
						} else {
							/*
							pbank.get(0).addnewLFlist(pfirm.get(0).id, pbank.get(0).buffer, rr * pfirm.get(0).lev + r,
									tau);
							pfirm.get(0).addnewBB(pbank.get(0).id, pbank.get(0).buffer, rr * pfirm.get(0).lev + r, tau);
							*/
							pbank.get(0).addLF(pfirm.get(0).id, pbank.get(0).buffer, rr * pfirm.get(0).lev + r,
									tau);
							pfirm.get(0).addBB(pbank.get(0).id, pbank.get(0).buffer, rr * pfirm.get(0).lev + r, tau);
							pfirm.get(0).addCandidate(pbank.get(0).id, rr * pfirm.get(0).lev + r, "Old", "Yes");

							for (CandidateBank cb : pfirm.get(0).Candidatelist) {
								if (cb.id == pbank.get(0).id) {
									cb.addPrincipal(pbank.get(0).buffer);
									if (cb.tradestate == "No")
										cb.tradestate = "Yes";
								}
							}
							creditmoney += pbank.get(0).buffer;
							pfirm.get(0).L -= pbank.get(0).buffer;
							grossr += pfirm.get(0).lev * 0.001 + r;
							rcounter++;
							pbank.get(0).buffer = 0;
							pbank.remove(0);

						}

					} else {
						pfirm.get(0).NGlist.add(pbank.get(0).id);
						pfirm.get(0).rejectordernum++;
						rf++;
					}
					/*else{
						pfirm.remove(0);
					}*/
				}

				iterator++;
				if (iterator > cmiterator)
					break;
				if (pfirm.size() == 0 || pbank.size() == 0)
					break;
			} while (iterator < cmiterator);
		}

		pfirm.clear();
		pbank.clear();

	}

	private void AdjustBS(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {
		// TODO Auto-generated method stub

		//firm setBS
		firm.stream().forEach(s -> s.setBS(fi, beta));

		//bank setBS
		//lfのみ調整(bb,lbはshorttermdで)
		bank.stream().forEach(s -> s.setBS1(expstate));

	}

	private void BankinputBuffer(ArrayList<Institution> bank, int x) {
		// TODO 自動生成されたメソッド・スタブ

		bank.stream().forEach(s -> {
			residual += s.buffer;
			s.residual = s.buffer;
			s.Residual.add(s.buffer);
		});
	}

	private void UpdateCMList(ArrayList<Institution> bank, ArrayList<Enterprise> firm, int x) {
		// TODO 自動生成されたメソッド・スタブ

		//CreditMarketでの使用リスト及び情報の整理
		firm.stream().forEach(s -> s.updateCMlist(x));
		bank.stream().forEach(s -> s.updateCMList());

	}

	private void FirmProductionProfit(ArrayList<Enterprise> firm, int x) {
		// TODO 自動生成されたメソッド・スタブ
		//生産活動
		for (int i = 0; i < firmnode; i++) {
			double p = 2 * Math.random();

			//firm.get(i).Y=firm.get(i).u*firm.get(i).Ylist.get(firm.get(i).Ylist.size()-1);
			firm.get(i).YY = firm.get(i).Y * p;
			//firm.get(i).Ylist.add(firm.get(i).Y);
			firm.get(i).pai = firm.get(i).YY;//ここで金利支払いのため実利益に生産高をあらかじめ足しておく
			grossproduction += firm.get(i).YY;
		}
		//System.out.println(grossproduction);
		GrossProduction.add(grossproduction);
	}

	private void FirmProductionProfit1(ArrayList<Enterprise> firm, int x) {

		//生産量の決定
		firm.stream().forEach(s -> s.setKY1(fi, beta));

		firm.stream().forEach(s -> {
			double u = 0.04 + 0.21 * Math.random();
			s.YY = u * s.Y;
			s.pai = s.YY;
			grossproduction += s.YY;
		});
		//System.out.println(grossproduction);
		GrossProduction.add(grossproduction);

	}

	private void FirmPayInstallments(ArrayList<Enterprise> firm, ArrayList<Institution> bank, ArrayList<BrProcess> vand,
			int x) {
		// TODO Auto-generated method stub

		for (int i = 0; i < firm.size(); i++) {
			for (int j = 0; j < firm.get(i).BB.size(); j++) {//
				for (int k = 0; k < bank.size(); k++) {
					if (firm.get(i).BB.get(j).bankid == bank.get(k).id) {
						double inst = firm.get(i).getInst(j);//+taucounterおよび支払い残高の計算
						double interestpayment = firm.get(i).getInterestpayment(j);
						double principal = firm.get(i).getPrincipal(j);
						firm.get(i).l -= principal;//もともと借りていた分の元本を返却
						firm.get(i).rit += interestpayment;//ステップtにおける金利の支払い
						creditmoney -= principal;
						for (CandidateBank cb : firm.get(i).Candidatelist) {
							if (cb.id == bank.get(k).id) {
								cb.minusPrincipal(principal);
							}
						}
					}
				}
				if (firm.get(i).BB.get(j).state == "Dead") {
					firm.get(i).l -= firm.get(i).getPrincipal(j);
					firm.get(i).rit += firm.get(i).getInterestpayment(j);
					vand.get(0).L += firm.get(i).getInst(j);//破綻処理機関に格納
					creditmoney -= firm.get(i).getPrincipal(j);
				}
			}
		}

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).LFlist.size(); j++) {
				bank.get(i).buffer += bank.get(i).LFlist.get(j).installment;
				//bank.get(i).n += bank.get(i).LFlist.get(j).interestpayment;
				bank.get(i).d += bank.get(i).LFlist.get(j).interestpayment;
				bank.get(i).pailf += bank.get(i).LFlist.get(j).interestpayment;
				bank.get(i).updateLFlist(j);
			}
		}

		//返済完了の場合、リストを消去
		for (int i = 0; i < firm.size(); i++) {
			for (int j = 0; j < firm.get(i).BB.size(); j++) {
				if (firm.get(i).getTaucounter(j) == firm.get(i).getTau(j)) {

					firm.get(i).BB.remove(j);
					j--;
				}
			}
		}

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).LFlist.size(); j++) {
				if (bank.get(i).getTaucounter(j) == bank.get(i).getTau(j)) {
					bank.get(i).LFlist.remove(j);
					j--;
				}

			}
		}

	}

	private void BankAdjustBuf() {
		Bank.stream().forEach(s->s.setBufferShortage());
	}

	private void FirmRealProfit1(ArrayList<Enterprise> firm, int x) {

		//自己資本金に応じて減少

		firm.stream().forEach(s -> {
			s.pai = s.YY - s.rit - s.f * s.A - stc;
		});

		//固定費
		/*
		firm.stream().forEach(s->{
			s.pai=s.YY-s.rit-5;}
		);
		*/

	}

	private void EvaLev(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {

		firm.stream().forEach(s -> s.EvaLeverage(sm));
		bank.stream().forEach(s -> s.EvaLeverage(sm));

	}

	private void BankruptFirm(ArrayList<Enterprise> firm, int x) {
		// TODO Auto-generated method stub
		for (int i = 0; i < firm.size(); i++) {
			if (firm.get(i).pai < 0) {
				firm.get(i).A = firm.get(i).A + firm.get(i).pai;
			} else {
				firm.get(i).A += (1 - pi) * firm.get(i).pai;
			}
			if (firm.get(i).A < fz) {
				firm.get(i).state = "Dead";
				//System.out.println("企業破綻したお");

			}
		}
		NumberOfFailedFirm.add(ffnum);//タームごと企業破綻数の格納
	}

	private void BankPayInstallments(ArrayList<Institution> bank, ArrayList<BrProcess> vand) {


		// TODO Auto-generated method stub

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).BBlist.size(); j++) {
				for (int k = 0; k < bank.size(); k++) {
					if (bank.get(i).BBlist.get(j).bankid == bank.get(k).id) {
						double inst = bank.get(i).getInst(j);
						double principal = bank.get(i).getPrincipal(j);

						moneystock -= principal;
						bank.get(i).bb -= principal;
						bank.get(i).buffer -= inst;
						bank.get(i).n -= inst - principal;
						bank.get(i).paibb += inst - principal;
						bank.get(i).updateBBlist(j);
					}
				}
				if (bank.get(i).BBlist.get(j).state == "Dead") {
					bank.get(i).bb -= bank.get(i).getPrincipal(j);
					bank.get(i).buffer -= bank.get(i).getInst(j);
					bank.get(i).n -= bank.get(i).getInst(j) - bank.get(i).getPrincipal(j);
					vand.get(0).L += bank.get(i).getInst(j);//破綻処理機関に格納
					bank.get(i).updateBBlist(j);
				}
			}
		}

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).LBlist.size(); j++) {
				for (int k = 0; k < bank.size(); k++) {
					if (bank.get(i).LBlist.get(j).bankid == bank.get(k).id) {
						bank.get(i).buffer += bank.get(i).LBlist.get(j).installment;
						bank.get(i).n += bank.get(i).LBlist.get(j).interestpayment;
						bank.get(i).pailb += bank.get(i).LBlist.get(j).interestpayment;
						bank.get(i).updateLBlist(j);

					}
				}
			}
		}

		//返済完了の場合、リストを消去，
		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).BBlist.size(); j++) {
				if (bank.get(i).getIBBTaucounter(j) == bank.get(i).getIBBTau(j)) {
					bank.get(i).BBlist.remove(j);
					j--;
				}
			}
		}

		for (int i = 0; i < bank.size(); i++) {
			for (int j = 0; j < bank.get(i).LBlist.size(); j++) {
				if (bank.get(i).getIBLTaucounter(j) == bank.get(i).getIBLTau(j)) {
					bank.get(i).LBlist.remove(j);
					j--;
				}
			}
		}
	}


	//予備
	int count;
	private void BankProfit(ArrayList<Institution> bank, ArrayList<Enterprise> firm, double r, int x, double div) {
		// TODO Auto-generated method stub

		//破綻行の影響（企業)
		if(expstate=="Build") {
		for (int i = 0; i < bank.size(); i++) {
			for (int k = 0; k < firm.size(); k++) {
				for (int j = 0; j < bank.get(i).LFlist.size(); j++) {
					if (bank.get(i).LFlist.get(j).firmid == firm.get(k).id) {
						if (firm.get(k).state == "Dead") {
							bank.get(i).badfloan += bank.get(i).getRemainingPrincipal(j);
							bank.get(i).grossbadloan += bank.get(i).getRemainingPrincipal(j);
							//System.out.println("企業貸出損失:"+bank.get(i).getRemainingPrincipal(j)+"　　破綻企業:"+firm.get(k).id);
							grossbadloan += bank.get(i).getRemainingPrincipal(j);
							creditmoney -= bank.get(i).getRemainingPrincipal(j);
							bank.get(i).lf -= bank.get(i).getRemainingPrincipal(j);
							bank.get(i).d -= bank.get(i).getRemainingPrincipal(j);
							bank.get(i).LFlist.remove(j);
							j--;
						}
					}
				}
			}
		}

		}else {
			for (int i = 0; i < bank.size(); i++) {
				for (int k = 0; k < firm.size(); k++) {
					for (int j = 0; j < bank.get(i).LFlist.size(); j++) {
						if (bank.get(i).LFlist.get(j).firmid == firm.get(k).id) {
							if (firm.get(k).state == "Dead") {
								bank.get(i).badfloan += bank.get(i).getRemainingPrincipal(j);
								bank.get(i).grossbadloan += bank.get(i).getRemainingPrincipal(j);
								//System.out.println("企業貸出損失:"+bank.get(i).getRemainingPrincipal(j)+"　　破綻企業:"+firm.get(k).id+"  "+bank.get(i).id+"銀行");
								count=bank.get(i).id;
								grossbadloan += bank.get(i).getRemainingPrincipal(j);
								creditmoney -= bank.get(i).getRemainingPrincipal(j);
								bank.get(i).lf -= bank.get(i).getRemainingPrincipal(j);
								bank.get(i).n -= bank.get(i).getRemainingPrincipal(j);
								bank.get(i).LFlist.remove(j);
								j--;
							}
						}
					}
				}
			}
		}

		//預金金利の支払い、外部資産金利の獲得
				bank.stream().forEach(s -> {
					s.setDepoOut(dr);
					s.setLiqAProfit(liqAr);
					s.pai = s.getPai(liqAr);
				});

				if(expstate == "Start") {
					//"build"のままの利益だと，利益額が大きすぎるので，配当として利益を減らす
					bank.stream().forEach(s -> s.setPayDiv(div));
		}

	}

	private void BankruptProcessing(ArrayList<Institution> bank, ArrayList<Enterprise> firm, int x) {
		// TODO 自動生成されたメソッド・スタブ

		for (int i = 0; i < firm.size(); i++) {
			if (firm.get(i).state == "Dead") {
				FailedFirm.add(firm.get(i));
				ffnum++;
				brfnum++;
				//System.out.println("企業破綻"+firm.get(i).id+" 借入金;"+firm.get(i).l+" 初期Candidate;"+firm.get(i).Candidatelistid);
				firm.remove(i);
				firmnode--;
				i--;
			}
		}

	}

	private void NewEntry(ArrayList<Enterprise> firm, ArrayList<Institution> bank, int x) {
		// TODO 自動生成されたメソッド・スタ

		//企業
		int Nentry = 0;
		double abs = Math.abs(d * (r - e));//市場金利は0.1を超えないことを想定

		//Nentry=(int) (200*0.2/(1+abs));
		Nentry = ffnum;
		//System.out.println("noob;"+ffnum);

		//企業リストへ追加
		//ランダム・パートナーチョイス型
		/*
		for(int i=0;i<Nentry;i++) {
			//取引先リストの作成
			ArrayList<Integer> list=new ArrayList<Integer>();//銀行idリストの作成
			for(int j=0;j<banknode;j++) list.add(bank.get(j).id);
			Collections.shuffle(list);
			ArrayList<Integer> Initbanklist=new ArrayList<Integer>();
			for(int k=0;k<initialbanknumber;k++) {
				Initbanklist.add(list.get(k));
			}
			Enterprise F=new Enterprise(newfirmid,k0,Initbanklist);
			firm.add(F);
			noobfirm++;
			firmnode++;
			newfirmid++;
		}
		*/

		//銀行企業固定
		for (int i = 0; i < Nentry; i++) {
			Enterprise F1 = new Enterprise(newfirmid, k0, FailedFirm.get(brfnum - Nentry).Candidatelistid, lfmin, lfmax,
					val, vi, f, maxmainbank, minmainbank);
			firm.add(F1);
			noobfirm++;
			firmnode++;
			newfirmid++;
		}

	}

	private void ChainOfBankruptBank(ArrayList<Institution> bank, int x, double r) {


		if(expstate == "Start") {
		BankruptProcessing br = new BankruptProcessing(Bank, Asset, shockflucrate, Firm);
		br.isStartAfterShortterm(brCAR, grossbadloan);
		bank.clear();
		bank.addAll(br.getBank());
		FailedBank.addAll(br.getDeadBank());
		DeadBank.addAll(br.getDeadBank());
		FbankCM.add(br.getFbankCM());
		getOutPutVariablesBR(br);
		br.AgentAndVariablesClear();

		bank.stream().forEach(s -> s.IBSInit(bank.size()));
		bank.stream().forEach(s -> s.setIBShortBS());
		}

	}

	private void BankRealProfit(ArrayList<Institution> bank, int x, double r) {
		// TODO 自動生成されたメソッド・スタブ

		Bank.stream().forEach(s -> s.setLongTermProfit(liqAr));
		Bank.stream().forEach(s -> grossbankprofit += s.pai);

	}

	private void BankBufferProcessing(ArrayList<Institution> bank, int x) {

		/*buffer<0の銀行は預金部分の減少，市場性資産の保有割合分まで減少
		 *
		 */

		bank.stream().filter(s -> s.buffer < 0)
				.forEach(s -> s.BufferProcessing());
		bank.stream().forEach(s -> grossbankbuffer += s.buffer);

		bank.stream().filter(s -> s.totalassets - s.totalinvest > 0.01 || s.totalinvest - s.totalassets > 0.01)
				.forEach(s -> System.out.println(s.id + "銀行釣り合ってないよ totalassets:"+s.totalassets+"  totalinvest : "+s.totalinvest));
	}

	private void InputOPList(int x) {
		// TODO 自動生成されたメソッド・スタブ
		//GrossBadLoan.add(grossbadloan);
		//FFnum.add(ffnum);
		//FFrate.add((double) ffnum / (firmnode - noobfirm));
		//AvgInterest.add(grossr / rcounter);
		//MoneyStock.add(moneystock);
		//CreditMoney.add(creditmoney);
		//Residual.add(residual);
		//RejectFinancing.add(rf);

		/*
		if (x == 0) {
			ProdGrowthRate.add(0.0);
		} else {
			ProdGrowthRate.add((GrossProduction.get(x) - GrossProduction.get(x - 1)) / GrossProduction.get(x - 1));
		}

		//企業
		Firm.stream().forEach(s -> grossfirmlev += s.lev);
		Avgfirmlev.add(grossfirmlev / Firm.size());
		*/
		/*
		//銀行
		Bank.stream().forEach(s -> grossbankclientnum += s.clientnum);
		Avgbankclientnum.add(grossbankclientnum / Bank.size());
		GrossBankProfit.add(grossbankprofit);
		GrossBankBuffer.add(grossbankbuffer);
		*/
	}

	private void InitOPVariables() {
		// TODO 自動生成されたメソッド・スタブ
		ffnum = 0;
		grossproduction = 0;
		grossbadloan = 0;
		noobfirm = 0;
		grossr = 0;
		rcounter = 0;
		residual = 0;
		grosssupply = 0;
		grossdemand = 0;
		rf = 0;
		grossfirmlev = 0;
		grossbankclientnum = 0;
		grossbankprofit = 0;
		grossbankbuffer = 0;
	}

	//一試行終了，結果の格納

	public void OutPutResult(ArrayList<OutPut> output, int atp) {
		// TODO 自動生成されたメソッド・スタブ

		//output.get(atp).Grossproduction.addAll(GrossProduction);
		//output.get(atp).Productgrowthrate.addAll(ProdGrowthRate);
		//output.get(atp).FFnum.addAll(FFnum);
		//output.get(atp).Avginterest.addAll(AvgInterest);
		//output.get(atp).Rejectfinancing.addAll(RejectFinancing);
		//企業
		//output.get(atp).Avgfirmlev.addAll(Avgfirmlev);
		//銀行
		//output.get(atp).Avgbankclientnum.addAll(Avgbankclientnum);
		//output.get(atp).Grossbankprofit.addAll(GrossBankProfit);
		//output.get(atp).Grossbankbuffer.addAll(GrossBankBuffer);

		//エージェント規模ごとにソート

		setSortFirmK();
		//setSortBankK();

		//Bank.stream().sorted(Comparator.comparing(Institution::getTotalassets));
		//Firm.stream().sorted(Comparator.comparingDouble(Enterprise::getK));

		output.get(atp).Finalbank.addAll(Bank);
		output.get(atp).Finalfirm.addAll(Firm);

	}

	//Expリストの初期化

	public void clearList() {
		// TODO 自動生成されたメソッド・スタブ
		GrossBadLoan.clear();
		FFnum.clear();
		FFrate.clear();
		AvgInterest.clear();
		MoneyStock.clear();
		CreditMoney.clear();
		Residual.clear();
		RejectFinancing.clear();
		Avgfirmlev.clear();
		Avgbankclientnum.clear();
		Grossdemand.clear();
		Grosssupply.clear();
		GrossProduction.clear();

		Prod.clear();
		ProdGrowthRate.clear();
		NumberOfFailedFirm.clear();
		FailedFirm.clear();
		FailedBank.clear();
		DeadBank.clear();
	}

	public ArrayList<Double> getGrossProduction() {
		// TODO 自動生成されたメソッド・スタブ
		return GrossProduction;
	}

	public int getalivefirm() {
		// TODO Auto-generated method stub
		return firmnode;
	}

	public int getAliveBank() {
		return banknode;
	}

	public void show() {
		// TODO 自動生成されたメソッド・スタブ
		/*
		for(int i=0;i<banknode;i++) {
			System.out.println("銀行"+Bank.get(i).id+"  企業取引数："+Bank.get(i).LFlist.size()+"  銀行取引数："+(Bank.get(i).LBlist.size()+Bank.get(i).BBlist.size())+"  自己資本比率:"+Bank.get(i).CAR);
			System.out.println("lftlist:"+Bank.get(i).lftlist);
		}*/
		System.out.println(Vand.get(0).L);
	}

	public int getTerm() {
		// TODO Auto-generated method stub
		return term;
	}

	public Double getAgrregateOP(int i) {
		// TODO Auto-generated method stub
		return GrossProduction.get(i);
	}

	public double getGrowthRateOP(int i) {
		// TODO 自動生成されたメソッド・スタブ
		if (i == 0)
			return 0;
		else {
			return (GrossProduction.get(i) - GrossProduction.get(i - 1)) / GrossProduction.get(i - 1);
		}
	}

	public int getFailedFirm(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return FFnum.get(i);
	}

	public double getBadLoan(int i) {

		return GrossBadLoan.get(i);
	}

	public double getFailedFirmrate(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return FFrate.get(i);
	}

	public double getBankCAR(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(j).CAR;
	}

	public double getFirmK(int j) {
		// TODO Auto-generated method stub

		return Firm.get(j).K;
	}

	public Double getAVGInterest(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return AvgInterest.get(j);
	}

	public double getMoneyStock(int j) {
		// TODO Auto-generated method stub
		return MoneyStock.get(j);
	}

	public double getCreditMoney(int j) {

		return CreditMoney.get(j);
	}

	public void setSortFirmK() {
		// TODO 自動生成されたメソッド・スタブ
		//総資産順に並び変え
		Collections.sort(Firm, new GetFirmKComparator());
	}

	private void setSortBankK() {
		// TODO 自動生成されたメソッド・スタブ
		//	Collections.sort(Firm,new GetBankKComparator());
	}

	public int getBankId(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).id;
	}

	public double getBankLf(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).lf;
	}

	public double getBankLb(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).lb;
	}

	public double getBankBb(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).bb;
	}

	public double getBankK(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).totalassets;
	}

	public double getBankBuffer(int i) {
		// TODO Auto-generated method stub
		return Bank.get(i).buffer;
	}

	public int getFirmCandidateNum(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Firm.get(j).Candidatelist.size();
	}

	public double getFirmLeverage(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Firm.get(j).lev;
	}

	public double getResidual(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Residual.get(j);
	}

	public double getBankGrossBadLoan(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).grossbadloan;
	}

	public double getBankResidual(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).residual;
	}

	public double getGrossDemand(int j) {
		// TODO Auto-generated method stub
		return Grossdemand.get(j);
	}

	public double getGrossSupply(int j) {
		// TODO Auto-generated method stub
		return Grosssupply.get(j);
	}

	public int getBankLFSize(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).LFlist.size();
	}

	public double getBankgoalLev(int j) {
		// TODO Auto-generated method stub
		return Bank.get(0).goallevt.get(j);
	}

	public Double getBankactLev2(int j) {
		// TODO Auto-generated method stub
		return Bank.get(0).CARlist.get(j);
	}

	public double getFirmnextLeverage(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Firm.get(j).nextlev;
	}

	public double getFirmLevNowEvalue(int i) {
		// TODO 自動生成されたメソッド・スタブ
		double levnowevalue = 0;
		for (int j = 0; j < Firm.get(i).levlist.size(); j++) {
			if (Firm.get(i).levlist.get(j).state == "Yes")
				levnowevalue = Firm.get(i).levlist.get(j).X;
		}
		return levnowevalue;
	}

	public double getBankLevlist(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(0).levlevellist.get(i);
	}

	public double getBanklevlevelvalue(int i) {
		// TODO 自動生成されたメソッド・スタブ
		//return Bank.get(0).levlist.get(i).evalue;
		return 0;
	}

	public double getBankTimeCAR(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(0).CARlist.get(j);
	}

	public double getBankLiqAsset(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return Bank.get(i).liqA;
	}

	public int getRejectFinancing(int j) {
		// TODO 自動生成されたメソッド・スタブ
		return RejectFinancing.get(j);
	}

	public int getBankClientNum(int j) {

		return Bank.get(j).clientnum;
	}

	public double getOneFirmLev(int ag, int j) {
		// TODO 自動生成されたメソッド・スタブ

		return Firm.get(ag).Levlist.get(j);
	}

	public double getOneFirmK(int ag, int j) {
		return Firm.get(ag).Klist.get(j);
	}

	public double getOneFirmGoalLev(int ag, int j) {
		return Firm.get(ag).goallevt.get(j);
	}

	public double getOneFirmCandNum(int ag, int j) {
		return Firm.get(ag).Candlist.get(j);
	}

	public double getOneFirmPai(int ag, int j) {
		return Firm.get(ag).pailist.get(j);
	}

	public double getOneFirmLevChoice(int ag, int j) {

		return Firm.get(ag).Choiceratelist.get(j);
	}

	public int getOneFirmLived(int ag) {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("live:" + Firm.get(ag).live);
		System.out.println("choiceratelist:" + Firm.get(ag).Choiceratelist.size());
		System.out.println("pailist:" + Firm.get(ag).pailist.size());
		System.out.println("Reallevlist:" + Firm.get(ag).RealLevlist.size());

		return Firm.get(ag).live;
	}

	public int getOneFirmId(int ag) {
		// TODO 自動生成されたメソッド・スタブ
		return Firm.get(ag).id;
	}

	public double getOneFirmRealLev(int ag, int j) {
		// TODO 自動生成されたメソッド・スタブ
		return Firm.get(ag).RealLevlist.get(j);
	}

	public int getFirmRejectOrderNum(int j) {
		// TODO Auto-generated method stub
		return Firm.get(j).rejectordernum;
	}

	public int getFirmNonBufferNum(int j) {
		// TODO Auto-generated method stub
		return Firm.get(j).nonbuffernum;
	}

	public ArrayList<Enterprise> getFinalResFirm(){
		return Firm;
	}



}