package logic;

import java.util.ArrayList;
import java.util.Iterator;

import bankagent.IBShort;
import bankagent.Institution;

public class IBshortMarket {

	ArrayList<Institution> bank = new ArrayList<Institution>();
	ArrayList<Institution> surplus = new ArrayList<Institution>();
	ArrayList<Institution> shortage = new ArrayList<Institution>();

	public double notrollover;
	public double ibsupply;

	public IBshortMarket(ArrayList<Institution> bank) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.bank.addAll(bank);
	}

	public void isStart() {
		//rolloverしない
		BankNotRollOver();
		bank.stream().forEach(s -> s.setIBShortBS());
		//surplus shortageに分類
		bankClassification();

		shortage.stream().forEach(s -> {
			//System.out.println("shortage: " + s.id + "  " + s.shortage);
		});
		int iterator = 0;
		//接続先との取引

		//bufferの取引
		do {
			Tranx("connected");
			iterator++;
			if (iterator > 10000 || surplus.size() == 0 || shortage.size() == 0)
				break;
		} while (iterator <= 10000);
		iterator = 0;
		//接続先外との取引

		//System.out.println("------notconnected---------");
			do {
			Tranx("notconnected");
			iterator++;
			if (iterator > 10000 || surplus.size() == 0 || shortage.size() == 0)
				break;
			} while (iterator < 10000);

	}

	public void BankNotRollOver() {
		//loan先のCAR
		bank.stream().forEach(s -> s.setIBLoanWithCAR(bank));
		//decide rollover
		//bank.stream().forEach(s->s.setIBRollOver(bank));

		for (Institution b : bank) {
			for (IBShort ibl : b.ibsloan) {
				if (ibl.CAR < b.CARD) {
					notrollover += ibl.money;
					//System.out.println(b.id+"銀行が "+ibl.id+"銀行NotRollOver:" + ibl.money);
					ibl.money = 0;
					for (Institution bb : bank) {
						if (ibl.id == bb.id) {
							for (IBShort ibb : bb.ibsborrow) {
								if (ibb.id == b.id) {
									ibb.money = 0;
								}
							}
						}
					}
				}
			}
		}
	}

	public void Tranx(String state) {

		//shr order作成
		shortage.stream().forEach(s -> s.setIBOrder(state));
		//srp recieveddorder作成
		for (Institution shr : shortage) {
			for (Institution srp : surplus) {
				if (shr.ibsorder.size() > 0) {
					if (shr.ibsorder.get(0).id == srp.id) {
						srp.setIBrecievedOrder(shr.id, shr.ibsorder.get(0).money, shr.CAR);
					}
				}
			}
		}
		//srp 貸出先の決定
		surplus.stream().forEach(s -> s.setIBresponseOrder());

		//取引
		for (Institution srp : surplus) {
			for (IBShort rorder : srp.ibsrecievedorder) {
				for (Institution shr : shortage) {
					if (rorder.id == shr.id) {
						if (rorder.money != 0) {
							ibsupply += rorder.money;
							//System.out.println("IBSupply:" + rorder.money + "  " + srp.id + "銀行から" + shr.id + "銀行");
							//System.out.println(srp.id + "銀行　buffer:" + srp.buffer + "  shortage ;" + srp.shortage);
							srp.addibsloan(rorder.id, rorder.money);
							shr.addibsborrow(srp.id, rorder.money);
						} else {
							shr.ibsrejectedbank.add(srp.id);
						}
					}
				}
			}
		}

		//agentlist更新
		Iterator<Institution> b = surplus.iterator();
		while (b.hasNext()) {
			Institution sur = b.next();
			if (sur.buffer == 0) {
				b.remove();
			}

		}

		Iterator<Institution> c = shortage.iterator();
		while (c.hasNext()) {
			Institution shr = c.next();
			if (shr.shortage == 0) {
				c.remove();
			}

		}
		//オーダーの消去
		bank.stream().forEach(s -> s.removeIBOrder());

	}

	public void bankClassification() {

		for (Institution b : bank) {
			if (b.buffer > 0.00001) {
				surplus.add(b);
			}
			if (b.shortage > 0.00001) {
				shortage.add(b);
			}
		}
	}

	public ArrayList<Institution> getBank() {
		return bank;
	}

	public double getNotRollOverAmount() {
		return notrollover;
	}

	public double getIBSupply() {
		return ibsupply;
	}

	public void AgentAndVariablesClear() {
		notrollover = 0;
		ibsupply = 0;
		bank.clear();
		surplus.clear();
		shortage.clear();
	}

}
