package home;

import java.util.ArrayList;

import calculator.Correlatin_Calculater;
import firmagent.Enterprise;

public class Correlation {
	
	public int i;//i試行目
	public int atp;
	
	public double coK_D,coD_Deg,coK_Deg;
	

	public Correlation(int i, int atp) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.i=i;
		this.atp=atp;
	}


	public void calcCorrelation(ArrayList<Enterprise> firm) {
		// TODO 自動生成されたメソッド・スタブ
		
		ArrayList<Double> K=new ArrayList<Double>();
		ArrayList<Double> D=new ArrayList<Double>();
		ArrayList<Double> DAR=new ArrayList<Double>();
		
		ArrayList<Double> Deg=new ArrayList<Double>();
		firm.stream().forEach(s->{
			K.add(s.K);
			D.add(s.l);
			Deg.add((double) s.mainbanknum);
		});
		Correlatin_Calculater corcalc=new Correlatin_Calculater();
		coK_D=corcalc.correlationCoefficient(K,D);
		coD_Deg=corcalc.correlationCoefficient(D,Deg);
		coK_Deg=corcalc.correlationCoefficient(K,Deg);
		
	}

}
