package calculator;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Random;

public class Utility {
	public static class MyRandom{
		private static Random random;

		static{
			random = new Random();
		}

		public static boolean nextBoolean(){
			return random.nextBoolean();
		}

		public static int nextInt(int bound){
			return random.nextInt(bound);
		}

		//sum of dblArray = 1
		public static int roulette(double[] dblArray){
			double sum = 0;

			double d = random.nextDouble();
			int i = 0;
			while(sum < d) sum += dblArray[i++];

			return i-1;
		}

		//element of intArray must be positive
		public static int[] nolRoulette(int[] intArray, int number){
			if(intArray.length < number) throw new IllegalArgumentException("MyRandom : nolRoulette");

			intArray = Arrays.copyOf(intArray, intArray.length);
			int[] index = new int[intArray.length];
			for(int i = 0; i < index.length; i++)
				index[i] = i;

			int sum, count, rnd, tmp; int[] nol = new int[number]; BitSet set = new BitSet();
			for(int t = 0; t < number; t++){
				sum = 0; count = 0; set.clear();
				for(int i = 0; i < intArray.length-t; i++) set.set((sum += intArray[i])-1);

				rnd = random.nextInt(set.length());
				for(int i = set.nextSetBit(0); i < rnd; i = set.nextSetBit(i+1)) count++;
				nol[t] = index[count];

				tmp = intArray[count];
				intArray[count] = intArray[intArray.length-(t+1)];
				intArray[intArray.length-(t+1)] = tmp;

				tmp = index[count];
				index[count] = index[index.length-(t+1)];
				index[index.length-(t+1)] = tmp;
			}

			return nol;
		}

		public static int[] nonOverlap(int bound, int number){
			int[] intArray = new int[bound];
			for(int i = 0; i < bound; i++)
				intArray[i] = i;

			int[] nol = new int[number]; int index, tmp;
			for(int i = 0; i < number; i++){
				nol[i] = intArray[index = random.nextInt(bound-i)];
				tmp = nol[i];
				intArray[index] = intArray[bound-(i+1)];
				intArray[bound-(i+1)] = tmp;
			}

			return nol;
		}
	}

	public static class MyCalendar{
		public static String get(){
			Calendar now = Calendar.getInstance();
			return Integer.toString(now.get(Calendar.YEAR))+Integer.toString(now.get(Calendar.MONTH)+1)+Integer.toString(now.get(Calendar.DATE))+Integer.toString(now.get(Calendar.HOUR_OF_DAY))+Integer.toString(now.get(Calendar.MINUTE))+Integer.toString(now.get(Calendar.SECOND));
		}
	}

	public static class Log{
		private static StringBuilder log;

		static{
			log = new StringBuilder();
		}

		public static void append(String str, boolean bl){
			if(bl) log.append(str+System.getProperty("line.separator"));
			else log.append(str);
		}

		public static void clear(){
			log = new StringBuilder();
		}

		/*
		public static void export(){
			try{
				File file = new File("log_" + Params.now + ".txt");
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));

				pw.println(log.toString());

				pw.close();

				clear();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		*/
	}
}

