package calculator;

import java.util.ArrayList;

import bankagent.IBShort;

public class VirtualNode {

	public int id;
	ArrayList<IBShort> indeg = new ArrayList<IBShort>();
	ArrayList<IBShort> outdeg = new ArrayList<IBShort>();
	public double car,n,totalassets;

	public double evalue,ev;//評価量,更新後評価量
	public String state,st;//nodeの状態 (U:Undistressed, D:Distressed, I:Inactive)
	public double w;//伝搬元からの枝の重み
	public double R;//debtrank


	public VirtualNode(int id, ArrayList<IBShort> ibsborrow, ArrayList<IBShort> ibsloan, double CAR, double n, double totalassets) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.indeg.addAll(ibsborrow);
		this.outdeg.addAll(ibsloan);
		this.car = CAR;
		this.n = n;
		this.totalassets = totalassets;
	}


	public void initState(double i, String string) {
		// TODO 自動生成されたメソッド・スタブ
		evalue = i;
		state  = string;
		w = 0.0;

		ev = evalue;
		st = state;

	}

	public void setCurrentState() {
		//System.out.println("setState "+id+"id evalue"+evalue+" state"+state);
		ev = evalue;
		st = state;
	}

	public void calcEdgeWeight(double e,double eivalue) {
		//重み：自己資本金ベース
		//w += Math.min(1 , e/n) * eivalue;
		double value = 0;
		if(totalassets*car<0.4) {
			value = 1;
		}else {
			value = e/(totalassets*car);
		}
		w += Math.min(1 , value) * eivalue;


		//重み：自己資本比率ベース
		/*
		if(totalassets * car -e < 0) {
			w += 1 * eivalue;
		}else {
			w += Math.min(1 , 0.04/((totalassets * car - e)/(totalassets - e))) * eivalue;
		}
		*/


	}

	public void calcNewEValue() {
		ev = Math.min(1, evalue + w);
		//System.out.println(id+"id ev"+ev+" state "+state);
	}

	public void updateCurrentState() {
		//System.out.println("updateCurState be id"+id+" state "+state+"  st"+st+" ev "+ev);
		if(ev >0 && state != "I") {st = "D";}
		else {
			if(state == "D") {st = "I";}
			else{
				st = state;}
			}

		//System.out.println("updateCurState af id"+id+" state "+state+"  st"+st+" ev "+ev);
	}

	public void updateState() {
		//System.out.println(id+"id st"+st+" ev"+ev);
		//System.out.println("updateState be "+id+"id state"+state+" evalue"+evalue+" st"+st);
		evalue = ev;
		if(state == "D") {state = "I";}
		else {
			state = st;
		}
		//System.out.println("updateState af "+id+"id state"+state+" evalue"+evalue+" st"+st);
	}

	public void addDebtRankValue(double value) {
		R += value;
	}

	public void setDebtRank(double nodesize) {
		R = R/nodesize;
	}

}
