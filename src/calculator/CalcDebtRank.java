package calculator;

import java.util.ArrayList;

import bankagent.IBShort;
import bankagent.Institution;

public class CalcDebtRank {

	/*
	 * 　幅優先探索(BFS : Breath-First Search)
	 */

	ArrayList<VirtualNode> vn = new ArrayList<VirtualNode>();
	//ArrayList<VirtualNode> v = new ArrayList<VirtualNode>();
	public boolean flag;//探索終了かどうか

	int counter;//デバッグ用カウンタ

	public void setVirtualNode(ArrayList<Institution> bank) {
		for(Institution b:bank) {
			VirtualNode VN = new VirtualNode(b.id,b.ibsborrow,b.ibsloan, b.CAR,b.n,b.totalassets);
			vn.add(VN);
		}
	}

	public CalcDebtRank(ArrayList<Institution> bank) {
		// TODO 自動生成されたコンストラクター・スタブ
		setVirtualNode(bank);
	}

	public void isStart() {

		//始点ノード
		for(VirtualNode initnode:vn) {
			//initialize
			Init(initnode.id);
			//System.out.println(initnode.id+"銀行-----------------------------------------------------");
			//traversing connect node
			while(flag != true) {
				counter++;
				System.out.println(counter);
				//System.out.println("////////////////////");
				//calc stress value
				for(VirtualNode n:vn) {
					//n.setCurrentState();
					if(n.state == "D") {
						for(IBShort indeg: n.indeg) {
							for(VirtualNode ni:vn) {
								if(ni.id == indeg.id && ni.state != "I") {
									//calc edge weight
									for(IBShort eji:ni.indeg) {
										for(VirtualNode nj: vn) {
											if(eji.id == nj.id && nj.state == "D") {
												ni.calcEdgeWeight(eji.money, nj.evalue);
											}
										}
									}
									//calc new evalue
									ni.calcNewEValue();
									//updata state
									ni.updateCurrentState();
								}
							}
						}
					}
				}
				//updating state
				for(VirtualNode n:vn) {
					n.updateState();
				}
				//loop-exit check
				flag = true;
				for(VirtualNode n:vn) {
					for(IBShort indeg:n.indeg) {
						for(VirtualNode ni:vn) {
							if(indeg.id == ni.id && ni.state == "D" && n.state == "U") {
								//System.out.println("false "+ni.id+"id  ni.state"+ni.state+"  "+n.id+"id n.state"+n.state);
								flag = false;
							}
						}
					}
				}
			}
			//reduction to calc debtrank
			for(VirtualNode n:vn) {
				if(n.id != initnode.id) initnode.addDebtRankValue(n.evalue);
			}
			//set final DebtRank
			initnode.setDebtRank(vn.size());
			counter = 0;
		}
	}

	public void isStart1() {
		//始点ノード
		for(VirtualNode initnode:vn) {
			//initialize
			Init(initnode.id);
			//System.out.println(initnode.id+"銀行-----------------------------------------------------");
			//traversing connect node
			while(flag != true) {
				counter++;
				//calc stress value
				for(VirtualNode ni:vn) {
					//sni.setCurrentState();
					if(ni.state == "D") {
					for(IBShort vj:ni.indeg) {
						for(VirtualNode nj:vn) {
							if(nj.id == vj.id) {
								nj.calcEdgeWeight(vj.money, ni.evalue);
								nj.calcNewEValue();
								nj.updateCurrentState();
							}
						}
					}
				}
				}
				//updating state
				for(VirtualNode n:vn) {
					if(n.state != "I") n.updateState();
				}
				//loop-exit check
				flag = true;
				for(VirtualNode ni:vn) {
					for(IBShort v:ni.indeg) {
						for(VirtualNode nj:vn) {
							if(nj.id == v.id && nj.state == "D") {
								flag = false;
								//System.out.println("false "+ni.id+"id  ni.state"+ni.state+"  "+nj.id+"id nj.state"+nj.state);
							}
						}
					}
				}
			}
			//reduction to calc debtrank
			for(VirtualNode n:vn) {
				if(n.id != initnode.id) initnode.addDebtRankValue(n.evalue);
			}
			//set final DebtRank
			initnode.setDebtRank(vn.size());
			counter = 0;
		}
	}

	public void Init(int initid) {
		//評価値，stateの初期化
		for(VirtualNode n:vn){
			if(n.id == initid) {n.initState(1.0,"D");}
			else {
			n.initState(0.0, "U");
			}
		}
		//flagの初期化
		flag = false;
	}

	public double getDebtRank(int id) {
		double res=0;
		for(VirtualNode n:vn) {
			if(n.id == id) res = n.R;
		}
		return res;
	}

	public void clealList() {
		// TODO 自動生成されたメソッド・スタブ
		vn.clear();
	}
}
