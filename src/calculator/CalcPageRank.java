package calculator;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFrame;

import bankagent.IBShort;
import bankagent.Institution;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.scoring.PageRank;
import edu.uci.ics.jung.graph.DirectedOrderedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;

public class CalcPageRank {

	Graph<Integer, Integer> g = new DirectedOrderedSparseMultigraph<Integer, Integer>();
	public double difprsum;
	private ArrayList<Double> difprlist = new ArrayList<Double>();

	public ArrayList<Institution> bank = new ArrayList<Institution>();



	public CalcPageRank(ArrayList<Institution> bank) {
		this.bank.addAll(bank);
	}

	public void calcDifPageRank() {
		//set PageRank Value and calc difference pagerank each bank
		setPageRank();
		// set to output dif pagerank list
		setDifPageRank();

	}

	public void setDifPageRank() {
		bank.stream().forEach(s -> difprlist.add(s.difpr));
		if(bank.size()==0) difprlist.add(0.0);
	}

	private void setPageRank() {
		// TODO 自動生成されたメソッド・スタブ

		int edgenum=0;
		for (Institution b:bank) {
			g.addVertex(b.id);
			for(IBShort lb : b.ibsloan) {
				g.addEdge(edgenum ,b.id, lb.id, EdgeType.DIRECTED);
				edgenum++;
			}
		}
		PageRank<Integer,Integer> pr = new PageRank<Integer,Integer>(g,0.15);
		pr.evaluate();
		for (Integer i : g.getVertices()) {
			// i <- b.id
			for(Institution b:bank) {
				if(b.id == i) b.setPageRank(pr.getVertexScore(i));
			}
	       // System.out.println(pr.getVertexScore(i));
		}
		//viz NW
		//showNW(g);
	}

	private void showNW(Graph<Integer, Integer> g) {
		// TODO 自動生成されたメソッド・スタブ
		Layout<Integer, Integer> layout = new CircleLayout<Integer, Integer>(g);
        layout.setSize(new Dimension(800,800));
        BasicVisualizationServer<Integer,Integer> vv =
            new BasicVisualizationServer<Integer,Integer>(layout);
        vv.setPreferredSize(new Dimension(850,850));

        JFrame frame = new JFrame("Simple Graph View");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(vv);
        frame.pack();
        frame.setVisible(true);
	}

	public void clearList() {
		difprlist.clear();
	}

	public Double getDifPageRank() {
		difprlist.stream().forEach(s-> difprsum+=s);
		return difprsum;
	}

	public Double getMaxDifPageRank() {
		// TODO 自動生成されたメソッド・スタブ
		return Collections.max(difprlist);
	}

	public Double getMinDifPageRank() {
		// TODO 自動生成されたメソッド・スタブ
		return Collections.min(difprlist);
	}


}
