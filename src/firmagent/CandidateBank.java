package firmagent;

public class CandidateBank implements Cloneable{

	public int id;
	public String rank;//(id)銀行のrank
	public double interest;
	public double grossl;//(id)銀行からの総借入金
	public double price;//(id)銀行からの総借入元本
	public String state;//新規先かリレーション先か
	public String tradestate;//交渉を行ったか否か

	public String Main;//メインバンクかどうか


	public CandidateBank(int bankid, double interest,String state,String tradestate) {
		// TODO Auto-generated constructor stub
		this.id=bankid;
		this.interest=interest;
		this.state=state;
		this.tradestate=tradestate;
		grossl=0;
	}


	public void minusPrincipal(double minusp){
		grossl-=minusp;
	}


	public void addPrincipal(double addp) {
		// TODO Auto-generated method stub
		grossl+=addp;
	}

	public CandidateBank clone() {
		try {
			CandidateBank cb=(CandidateBank) super.clone();
			return cb;
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}

}
