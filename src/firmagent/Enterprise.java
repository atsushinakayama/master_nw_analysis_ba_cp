package firmagent;

import java.util.ArrayList;
import java.util.Iterator;

public class Enterprise implements Cloneable{

	public int id;//id
	public double K0;//初期自己資本

	//初期自己資本あり
	public double fCAR;
	public double a0;//初期自己資本金
	public double l0;//初期他人資本

	public int live;

	public Enterprise(double f, int i, double k0) {
		// TODO Auto-generated constructor stub
		this.id = i;
		this.K0 = k0;
		this.f = f;
		setinitBS();
		live = 0;
	}

	private void setinitBS() {
		// TODO 自動生成されたメソッド・スタブ
		this.A = K0;
		this.K = K0;
		lev = K / A;

	}

	//初期自己資本比率ありコンストラクタ
	public Enterprise(int i, double k0, double fCAR) {
		// TODO Auto-generated constructor stub
		this.id = i;
		this.K0 = k0;
		this.fCAR = fCAR;

		setBS1(k0, fCAR);
	}

	private void setBS1(double k0, double fCAR) {
		// TODO Auto-generated method stub
		A = k0 * fCAR;
		L0 = k0 - A;
		K = k0;
	}

	//newentryコンストラクタ
	public Enterprise(int newfirmid, double k0, ArrayList<Integer> initbanklist, double lfmin, double lfmax, double val,
			double vi,double f,int maxmainbank,int minmainbank) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = newfirmid;
		this.K0 = k0;
		this.Candidatelistid.addAll(initbanklist);
		this.f=f;
		this.maxmainbank=maxmainbank;
		this.minmainbank=minmainbank;

		setinitBS();
		String state = "old";
		String tradestate = "no";

		String nstate = "No";
		String range = "No";

		for (int i = 0; i < initbanklist.size(); i++) {
			addCandidate(initbanklist.get(i), 0, state, tradestate);
		}

		setLevList(lfmin, lfmax, val, nstate, range, vi);
		DecideFirstLev();
		live = -1;
		nearlev=1.0;
	}

	public String state;//現存か破綻

	public double l;//総借入金
	public double lp;//candidatelistの借入元本総和

	public int levnownum;//現在のレバレッジナンバー
	public double nextlev;//次期選択レバレッジ
	public double reallev;//資金供給後のレバレッジ
	public double sigmat;
	public double counter;//レバレッジ選択確率カウンター
	public double nearlev;
	public double minlevel = 0;//Reallev用nearlevlevel

	public double upai;//期待利益
	public double pai;//実利益
	public double Y;//予想生産高
	public double YY;//実生産高
	public double rit;//ステップtでの金利の支払い分
	public double A;//自己資本
	public double K;//総資本
	public double KK;//最適資本K*
	public double ps;//新規partnerchoice確率
	public double L;//希望借入金
	public double lev;//レバレッジA
	public double L0;//他人資本
	public double f;//固定費
	public double gr;//成長率
	public int maxmainbank;//銀行最大取引数
	public int minmainbank;//少なくとも取引できる銀行数
	public double relval;//企業群の中での相対値

	public int initbank;//初期取引銀行
	public int mainbanknum;//取引先銀行数（最終)

	public int rejectordernum;//オーダーを拒否された回数
	public int nonbuffernum;//buffer不足に資金需要が余った回数

	public ArrayList<Leverage> levlist = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<LevProbably> list = new ArrayList<LevProbably>();//レバレッジ最終候補リスト
	public ArrayList<Double> goallevt = new ArrayList<Double>();//タイムステップごと目標レバレッジ
	public ArrayList<Double> actlevt = new ArrayList<Double>();//タイムステップごと実際のレバレッジ(生産活動前)

	public ArrayList<Double> pailist = new ArrayList<Double>();//実利益リスト
	public ArrayList<Double> Ylist = new ArrayList<Double>();//生産高リスト
	public ArrayList<Double> Alist = new ArrayList<Double>();//自己資本高リスト

	public ArrayList<CandidateBank> Candidatelist = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<Integer> Candidatelistid = new ArrayList<Integer>();//取引先の候補リスト（idのみ)
	public ArrayList<Integer> Tradeable = new ArrayList<Integer>();//取引可能な金融機関のリスト(idのみ)

	public ArrayList<OrderList> Order = new ArrayList<OrderList>();//marketに出すオーダー（一つのみ）

	public ArrayList<BorrowingBank> BB = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト
	public ArrayList<BorrowingBank> newBB = new ArrayList<BorrowingBank>();//t期に銀行から借り入れたリスト
	public BorrowingBank brbank;
	public ArrayList<Integer> NGlist = new ArrayList<Integer>();//交渉を行った銀行リスト(次cd市場の交渉ができない)

	//時系列格納リスト
	public ArrayList<Double> Levlist = new ArrayList<Double>();
	public ArrayList<Double> RealLevlist = new ArrayList<Double>();
	public ArrayList<Double> Klist = new ArrayList<Double>();
	public ArrayList<Integer> Candlist = new ArrayList<Integer>();
	public ArrayList<Double> ROAlist = new ArrayList<Double>();
	public ArrayList<Double> Choiceratelist = new ArrayList<Double>();
	public ArrayList<Integer> RejectOrderNumlist = new ArrayList<Integer>();//オーダー拒否数
	public ArrayList<Integer> NonBufferNumlist = new ArrayList<Integer>(); //buuferが足りないから融資を受けれない数(全額受けれなかった場合)

	//CloneList
	public ArrayList<Leverage> levlist1 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist1 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB1 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist2 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist2 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB2 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist3 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist3 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB3 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist4 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist4 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB4 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist5 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist5 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB5 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist6 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist6 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB6 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト

	public ArrayList<Leverage> levlist7 = new ArrayList<Leverage>();//レバレッジ候補リスト
	public ArrayList<CandidateBank> Candidatelist7 = new ArrayList<CandidateBank>();//取引先の候補リスト(id,金利)
	public ArrayList<BorrowingBank> BB7 = new ArrayList<BorrowingBank>();//銀行からの借り入れリスト



	public void setShorttermBB(int failedbankid) {
		for (BorrowingBank bb : BB) {
			if (bb.bankid == failedbankid) {
				bb.state = "Dead";
			}
		}
	}

	public void setMaxMainBank(int maxbank) {
		maxmainbank=maxbank;
	}

	public void setMinMainBank(int minbank) {
		minmainbank=minbank;
	}

	public void setRelativeValue(double counter) {
		relval=(K/counter);
	}

	public void setMaxMainBankNum(int banknum) {
		maxmainbank=(int)(Math.ceil(banknum*relval));
		if(maxmainbank<minmainbank) maxmainbank=minmainbank;
		//maxmainbank=100;
	}

	public double getK() {
		return K;
	}

	public void DecideFirstLev() {
		levlist.get(0).state = "Yes";
	}

	public void calcEValue(double h) {
		levlist.stream().forEach(s -> {
			s.evalue -= h * s.evalue;
			s.setStrength();
		});
	}

	public void setAdjacent() {

		for (int i = 0; i < levlist.size(); i++) {
			if (levlist.get(i).state == "Yes") {
				levlist.get(i).adjacent = "Yes";
				if (i > 0)
					levlist.get(i - 1).adjacent = "Yes";
				if (i < levlist.size() - 1)
					levlist.get(i + 1).adjacent = "Yes";

			}
		}

	}

	public void setAvaRange() {

		/*
		//現在のレバレッジに近い値をstate=Yesに
		double a=100;
		for(Leverage l:levlist) {
			if(Math.abs(a)>Math.abs(l.level-lev)) {
				a=Math.abs(l.level-lev);
				nearlev=l.level;
			}
		}
		levlist.stream().filter(s->s.level==nearlev)
						.forEach(s->{levnownum=s.levnum;
						s.state="Yes";
								});
		*/

		levlist.stream().filter(s -> s.state == "Yes")
				.forEach(s -> levnownum = s.levnum);

		int i;
		i = levnownum - 3;
		if (i < 0)
			i = 0;

		for (int j = i; j <= levnownum + 3; j++) {
			if (j >= levlist.size()) {
				break;
			} else {
				levlist.get(j).range = "Yes";
			}
			if (levlist.get(j).adjacent == "Yes")
				levlist.get(j).finalrange = "Yes";

		}

		int count = 0;
		int maxrangenum = 0;
		for (int j = 0; i < levlist.size(); i++) {
			if (levlist.get(i).range == "Yes")
				maxrangenum = levlist.get(i).levnum;
			if (levlist.get(i).finalrange == "Yes")
				count++;
		}

		if (count == 0) {
			nextlev = levlist.get(maxrangenum).levnum;
			levlist.get(maxrangenum).state = "Yes";
		}

	}

	public void choiceLev() {

		levlist.stream().filter(s -> s.finalrange == "Yes")
				.forEach(s -> sigmat += Math.exp(s.X));

		levlist.stream().filter(s -> s.finalrange == "Yes")
				.forEach(s -> {
					s.pl = Math.exp(s.X) / sigmat;
					counter += s.pl;
					LevProbably l = new LevProbably(s.levnum, counter, s.level);
					list.add(l);
				});

		double r = Math.random();//乱数生成
		double a = 0;

		//前期に選択したレバレッジを現在のレバレッジとしている
		/*
		for(LevProbably l:list) {
			if(r<=l.getCount()&&a<=r) {
				nextlev=l.getLevel();
				Choiceratelist.add(l.getCount()-a);
				break;
			}else {
				a=l.getCount();
			}

		}*/

		//前stepCM後に一番近かったレバレッジレベルを前期レバレッジとする

		for (int i = 0; i < list.size(); i++) {
			if (r <= list.get(i).getCount() && a <= r) {
				nextlev = list.get(i).getLevel();
				if (i == 0||live==0) {
					Choiceratelist.add(list.get(i).getCount());
				} else {
					Choiceratelist.add(list.get(i).getCount() - list.get(i - i).getCount());
				}
				break;
			} else {
				a = list.get(i).getCount();
			}
		}

		goallevt.add(nextlev);

		levlist.stream().filter(s -> s.level == nextlev)
				.forEach(s -> s.state = "Yes");
		levlist.stream().filter(s -> s.level != nextlev)
				.forEach(s -> s.state = "No");
		for (Leverage l : levlist) {
			if (l.level - 0.5 == nextlev || l.level + 0.5 == nextlev) {
				l.adjacent = "Yes";
			} else {
				l.adjacent = "No";
			}
		}

	}

	public void setDemand() {

		L = nextlev * A - K;
		if (L < 0.01)
			L = 0;
	}

	public void EvaLeverage(double sm) {

		levlist.stream().filter(s -> s.state == "Yes")
				.forEach(s -> s.evalue = (1 - sm) * s.evalue + sm * pai);

	}

	public void addBB(int Bankid, double price, double interest, int tau) {
		BorrowingBank bb = new BorrowingBank(Bankid, price, interest, tau);
		BB.add(bb);

	}

	public void addCandidate(int bankid, double interest, String Old, String tradestate) {
		CandidateBank cb = new CandidateBank(bankid, interest, Old, tradestate);
		Candidatelist.add(cb);
	}

	public void addOrderList(int bankid, double l, double interest, int tau) {
		OrderList order = new OrderList(bankid, l, interest, tau);
		Order.add(order);
	}

	public double getInst(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		double payment = BB.get(bankj).getPayment();
		return payment;
	}

	public double getInterestpayment(int j) {
		double payment=BB.get(j).getInterestPayment();
		return payment;
	}

	public void setInterestPayment() {
		for(BorrowingBank bb:BB) {
			rit+=bb.getInterestPayment();
		}
	}

	public double getPrincipal(int bankj) {
		// TODO Auto-generated method stub

		return BB.get(bankj).getPrincipal();
	}

	public int getTaucounter(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		return BB.get(bankj).getTaucounter();
	}

	public int getTau(int bankj) {
		// TODO 自動生成されたメソッド・スタブ
		return BB.get(bankj).getTau();
	}

	public void addnewBB(int bankid, double price, double interest, int tau) {
		// TODO 自動生成されたメソッド・スタブ
		BorrowingBank bb = new BorrowingBank(bankid, price, interest, tau);
		newBB.add(bb);
	}

	public void Init() {
		// TODO 自動生成されたメソッド・スタブ
		upai = 0;
		pai = 0;
		rit = 0;
		Y = 0;
		L = 0;
		sigmat = 0;
		counter = 0;
		lp = 0;
		rejectordernum = 0;
		nonbuffernum = 0;
	}

	public void setBS(double fi, double beta) {
		// TODO 自動生成されたメソッド・スタブ

		l = 0;
		BB.stream().forEach(s -> l += s.getRemainingprincipal());

		K = A + l;
		//Y=fi*K;
		Y = fi * Math.pow(K, beta);//予想生産高
		lev = K / A;
	}

	public void setKY1(double fi, double beta) {
		K = A + l + L0;

		lev = K / A;

		//生産活動前の実際のレバレッジを格納
		actlevt.add(lev);
	}

	public void setExpectedProfit(double g) {
		// TODO 自動生成されたメソッド・スタブ
		upai = (1 - g) * Y - rit;
	}

	public void setKK(double g, double c, double fi, int x) {
		// TODO 自動生成されたメソッド・スタブ
		KK = (((1 - g) / c) - ((rit - Alist.get(Alist.size() - 1)) / 2)) / (g * fi);
		/*
		BigDecimal bd=new BigDecimal(KK);
		BigDecimal bd1=bd.setScale(1, BigDecimal.ROUND_HALF_UP);
		KK=bd1.doubleValue();
		*/
	}

	public void updatePList() {
		// TODO 自動生成されたメソッド・スタブ
		Ylist.add(Y);
		Alist.add(A);
		pailist.add(pai);
		Levlist.add(lev);
		Klist.add(K);
		if (Klist.size() > 1)
			gr = Klist.get(Klist.size() - 1) / Klist.get(Klist.size() - 2);

		Candlist.add(mainbanknum);

		if (Order.size() != 0)
			Order.clear();

	}

	public void updateCMlist(int x) {
		// TODO 自動生成されたメソッド・スタブ

		//CMリストの消去
		Order.clear();

		//取引を行っている状態ならtradestateをYesに

		if (x > 3)
			Candidatelist.stream().forEach(s -> s.tradestate = "No");

		for (CandidateBank cb : Candidatelist) {
			for (BorrowingBank bb : newBB) {
				if (cb.id == bb.bankid)
					cb.tradestate = "Yes";
			}
			for (BorrowingBank bbb : BB) {
				if (cb.id == bbb.bankid)
					cb.tradestate = "Yes";
			}
		}

		//candidatelistの中で取引を行っていない銀行の消去
		Iterator<CandidateBank> itcd = Candidatelist.iterator();
		while (itcd.hasNext()) {

			String tradestate = itcd.next().tradestate;
			if (tradestate == "No") {
				itcd.remove();
			}
		}

		//candidatelistのpriceの更新(credit市場でいちいち足さなくてよい)
		Candidatelist.stream().forEach(s -> s.price = 0);

		for (CandidateBank cb : Candidatelist) {
			for (BorrowingBank bb : BB) {
				if (cb.id == bb.bankid) {
					cb.price += bb.price;
				}
			}
			for (BorrowingBank bbb : newBB) {
				if (cb.id == bbb.bankid) {
					cb.price += bbb.price;
				}
			}
		}

		//mainbank数の計算
		ArrayList<Integer> mainbank = new ArrayList<Integer>();
		Candidatelist.stream().filter(s -> !mainbank.contains(s.id))
				.forEach(s -> mainbank.add(s.id));
		mainbanknum = mainbank.size();

		//NGlist初期化
		NGlist.clear();

		//RealLeverageの格納
		SetRealLev();

		//オーダー拒否の格納
		RejectOrderNumlist.add(rejectordernum);
		NonBufferNumlist.add(nonbuffernum);
	}

	public void SetRealLev() {

		reallev = lev;
		RealLevlist.add(reallev);
		//現在のレバレッジに近い値のレベルのstateをyesに
		levlist.stream().forEach(s -> s.state = "No");
		double min = 0, now = 0;
		for (Leverage l : levlist) {
			now = Math.abs(reallev - l.level);
			if (min - now >= 0) {
				min = now;
				minlevel = l.level;
			}
		}
		levlist.stream().filter(s -> s.level == minlevel)
				.forEach(s -> s.state = "Yes");

	}

	public void updateLevlist() {

		levlist.stream().forEach(s -> {
			s.range = "No";
			s.finalrange = "No";
			s.adjacent = "No";
			s.pl = 0;
		});
		list.clear();

	}

	public void setLevList(double lfmin, double lfmax, double val, String state, String range, double vi) {
		// TODO 自動生成されたメソッド・スタブ

		int size = (int) ((lfmax - lfmin) / val) + 1;
		double value = lfmin;
		for (int i = 0; i < size; i++) {
			Leverage newlev = new Leverage(value, 0, 0, state, range, i, vi);
			levlist.add(newlev);
			value += val;
		}

	}

	public void setCloneList() {
		levlist.stream().forEach(s->levlist1.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist1.add(s.clone()));
		BB.stream().forEach(s->BB1.add(s.clone()));
	}

	public void addCloneList() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist1.stream().forEach(s->levlist.add(s));
		Candidatelist1.stream().forEach(s->Candidatelist.add(s));
		BB1.stream().forEach(s->BB.add(s));
		levlist1.clear();Candidatelist1.clear();BB1.clear();
	}

	public void setCloneList1() {
		levlist.stream().forEach(s->levlist2.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist2.add(s.clone()));
		BB.stream().forEach(s->BB2.add(s.clone()));
	}

	public void addCloneList1() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist2.stream().forEach(s->levlist.add(s));
		Candidatelist2.stream().forEach(s->Candidatelist.add(s));
		BB2.stream().forEach(s->BB.add(s));
		levlist2.clear();Candidatelist2.clear();BB2.clear();
	}

	public void setCloneList2() {
		levlist.stream().forEach(s->levlist3.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist3.add(s.clone()));
		BB.stream().forEach(s->BB3.add(s.clone()));
	}

	public void addCloneList2() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist3.stream().forEach(s->levlist.add(s));
		Candidatelist3.stream().forEach(s->Candidatelist.add(s));
		BB3.stream().forEach(s->BB.add(s));
		levlist3.clear();Candidatelist3.clear();BB3.clear();
	}

	public void setCloneList3() {
		levlist.stream().forEach(s->levlist4.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist4.add(s.clone()));
		BB.stream().forEach(s->BB4.add(s.clone()));
	}

	public void addCloneList3() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist4.stream().forEach(s->levlist.add(s));
		Candidatelist4.stream().forEach(s->Candidatelist.add(s));
		BB4.stream().forEach(s->BB.add(s));
		levlist4.clear();Candidatelist4.clear();BB4.clear();
	}

	public void setCloneList4() {
		levlist.stream().forEach(s->levlist5.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist5.add(s.clone()));
		BB.stream().forEach(s->BB5.add(s.clone()));
	}

	public void addCloneList4() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist5.stream().forEach(s->levlist.add(s));
		Candidatelist5.stream().forEach(s->Candidatelist.add(s));
		BB5.stream().forEach(s->BB.add(s));
		levlist5.clear();Candidatelist5.clear();BB5.clear();
	}

	public void setCloneList5() {
		levlist.stream().forEach(s->levlist6.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist6.add(s.clone()));
		BB.stream().forEach(s->BB6.add(s.clone()));
	}

	public void addCloneList5() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist6.stream().forEach(s->levlist.add(s));
		Candidatelist6.stream().forEach(s->Candidatelist.add(s));
		BB6.stream().forEach(s->BB.add(s));
		levlist6.clear();Candidatelist6.clear();BB6.clear();
	}


	public void setCloneList6() {
		levlist.stream().forEach(s->levlist7.add(s.clone()));
		Candidatelist.stream().forEach(s->Candidatelist7.add(s.clone()));
		BB.stream().forEach(s->BB7.add(s.clone()));
	}

	public void addCloneList6() {
		levlist.clear();Candidatelist.clear();BB.clear();
		levlist7.stream().forEach(s->levlist.add(s));
		Candidatelist7.stream().forEach(s->Candidatelist.add(s));
		BB7.stream().forEach(s->BB.add(s));
		levlist7.clear();Candidatelist7.clear();BB7.clear();
	}

	public Enterprise clone() {
		try {
			Enterprise f=(Enterprise) super.clone();
			f.levlist=new ArrayList<Leverage>(levlist);
			f.list=new ArrayList<LevProbably>(list);
			f.goallevt=new ArrayList<Double>(goallevt);
			f.actlevt=new ArrayList<Double>(actlevt);
			f.pailist=new ArrayList<Double>(pailist);
			f.Ylist=new ArrayList<Double>(Ylist);
			f.Alist=new ArrayList<Double>(Alist);
			f.Candidatelistid=new ArrayList<Integer>(Candidatelistid);
			f.Candidatelist=new ArrayList<CandidateBank>(Candidatelist);
			f.Tradeable=new ArrayList<Integer>(Tradeable);
			f.Order=new ArrayList<OrderList>(Order);
			f.BB=new ArrayList<BorrowingBank>(BB);
			f.NGlist=new ArrayList<Integer>(NGlist);
			f.Levlist=new ArrayList<Double>(Levlist);
			f.RealLevlist=new ArrayList<Double>(RealLevlist);
			f.Klist=new ArrayList<Double>(Klist);
			f.ROAlist=new ArrayList<Double>(ROAlist);
			f.Choiceratelist=new ArrayList<Double>(Choiceratelist);
			f.RejectOrderNumlist=new ArrayList<Integer>(RejectOrderNumlist);
			f.NonBufferNumlist=new ArrayList<Integer>(NonBufferNumlist);
			return f;

		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}





}
