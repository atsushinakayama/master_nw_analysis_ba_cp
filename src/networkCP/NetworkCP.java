package networkCP;

import java.util.ArrayList;
import java.util.Collections;

//コアペリフェラルネットワークの作成

public class NetworkCP {

	public int banknode;
	public MatrixCP matrix;
	public double a1, a2;//large,medium,small銀行の割合
	public ArrayList<Virtual> agent = new ArrayList<Virtual>();//ネットワーク作成用
	public int minibCN = 4;//中小銀行グループ内での完全グラフの行数
	public double randIn, randOut;//つながっていない銀行とランダムに接続する確率
	public double disIn = 0.3, disOut = 0.3;//disassortative

	public void createAgent(int banknode) {
		for (int i = 1; i <= banknode; i++) {
			Virtual vir = new Virtual(i, banknode);
			agent.add(vir);
			if (i <= banknode * a1) {
				agent.get(i - 1).rank = "mega";
			} else {
				if (i <= banknode * a2) {
					agent.get(i - 1).rank = "medium";
				} else {
					agent.get(i - 1).rank = "small";
				}
			}
		}
	}

	public NetworkCP(int banknode, double a1, double a2, double randIn, double randOut) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.banknode = banknode;
		matrix = new MatrixCP(banknode, false);
		this.a1 = a1;
		this.a2 = a2;
		this.randIn = randIn;
		this.randOut = randOut;

		//仮想エージェントを作りネットワークを作成
		//ランク分け
		createAgent(banknode);
	}

	public void createCP1() {
		ArrayList<Virtual> mega=new ArrayList<Virtual>();
		ArrayList<Virtual> medium=new ArrayList<Virtual>();
		ArrayList<Virtual> small=new ArrayList<Virtual>();
		int alloc = (int) (banknode * a1);//大銀行の数(グループ数)
		int mem = (int) (banknode * (1 - a1) / alloc);//1グループの中小銀行の数
		for(Virtual v:agent) {
			if(v.rank=="mega") mega.add(v);
			if(v.rank=="medium") medium.add(v);
			if(v.rank=="small") small.add(v);
		}
		//大銀行完全ネットワークの作成
		for(Virtual m:mega) {
			for(Virtual mm:mega) {
				if(m.id!=mm.id) {
					m.Inid.add(mm.id);
					m.Outid.add(mm.id);
				}
			}
		}
		//大銀行ー中・小銀行グループ化
		int mediumalloc=medium.size()/mega.size();//1グループあたりの中銀行
		int smallalloc=small.size()/mega.size();//1グループあたりの小銀行
		CreateGroup(mediumalloc,medium,mega);
		CreateGroup(smallalloc,small,mega);


	}

	public void CreateGroup(int alloc,ArrayList<Virtual> agent,ArrayList<Virtual> mega) {
		int i=1,counter=1;
		for(Virtual s:agent) {
			s.groupid=i;
			counter++;
			if(counter==alloc) {
				i++;counter=1;
			}
		}

	}

	public void createCP() {
		// TODO 自動生成されたメソッド・スタブ
		int alloc = (int) (banknode * a1);//大銀行の数(グループ数)
		int mem = (int) (banknode * (1 - a1) / alloc);//1グループの中小銀行の数

		//大銀行完全ネットワークの作成
		for (int i = 0; i < banknode; i++) {
			if (i < alloc) {
				for (int j = 0; j < alloc; j++) {
					if (agent.get(i).id != j + 1) {
						agent.get(i).Inid.add(j + 1);
						agent.get(i).Outid.add(j + 1);
					}
				}
			}
		}

		//中小銀行と大銀行を一対一で割り当て(9このグループに分ける)
		int count = alloc;
		int count1 = alloc + mem;
		for (int j = 1; j <= alloc; j++) {//大銀行id
			for (int i = count; i < count1; i++) {
				agent.get(i).Inid.add(j);
				agent.get(i).Outid.add(j);
			}
			count += mem;
			count1 += mem;
		}

		//中小銀行の中でクラスタリング
		for (int i = 1; i <= alloc; i++) {
			ArrayList<Virtual> list = new ArrayList<Virtual>();
			for (int j = alloc; j < banknode; j++) {
				if (agent.get(j).Inid.get(0) == i) {
					list.add(agent.get(j));
				}
			}

			makeCluster(list);
			list.clear();
		}

		//つながっていない銀行とランダムに接続(InidとOutidそれぞれ)
		RandomConnect();

		/*
		 * medium銀行の次数を上げたい
		 * コア周辺ではmedium銀行がクラスターを形成すればよい?
		 */

		//Disassoratative
		//Disassoratative();
		connectMegaandMedium();
		connectMegaandSmall();


	}

	private void connectMegaandSmall() {
		ArrayList<Virtual> mega = new ArrayList<Virtual>();
		ArrayList<Virtual> small = new ArrayList<Virtual>();

		for (Virtual agent : agent) {
			if (agent.rank == "mega") {
				mega.add(agent);
			} else {
				if(agent.rank=="small") {
					small.add(agent);
				}
			}
		}
		//Inid,Outidそれぞれ
		for (Virtual m : mega) {
			for (Virtual o : small) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if (!m.Inid.contains(o.id) && rndIn < disIn) {
					m.Inid.add(o.id);
					o.Outid.add(m.id);

				}
				if (!m.Outid.contains(o.id) && rndOut < disIn) {
					m.Outid.add(o.id);
					o.Inid.add(m.id);
				}

			}
		}

		mega.clear();
		small.clear();


	}

	private void connectMegaandMedium() {
		ArrayList<Virtual> mega = new ArrayList<Virtual>();
		ArrayList<Virtual> medium = new ArrayList<Virtual>();

		for (Virtual agent : agent) {
			if (agent.rank == "mega") {
				mega.add(agent);
			} else {
				if(agent.rank=="medium") {
					medium.add(agent);
				}
			}
		}
		//Inid,Outidそれぞれ
		for (Virtual m : mega) {
			for (Virtual o : medium) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if (!m.Inid.contains(o.id) && rndIn < disIn) {
					m.Inid.add(o.id);
					o.Outid.add(m.id);

				}
				if (!m.Outid.contains(o.id) && rndOut < disIn) {
					m.Outid.add(o.id);
					o.Inid.add(m.id);
				}

			}
		}

		mega.clear();
		medium.clear();



	}

	private void Disassoratative() {
		//大銀行グループと中小銀行グループに分け，ランダムにつなぐ

		ArrayList<Virtual> mega = new ArrayList<Virtual>();
		ArrayList<Virtual> other = new ArrayList<Virtual>();

		for (Virtual agent : agent) {
			if (agent.rank == "mega") {
				mega.add(agent);
			} else {
				other.add(agent);
			}
		}

		//Inid,Outidそれぞれ
		for (Virtual m : mega) {
			for (Virtual o : other) {
				double rndIn = Math.random();
				double rndOut = Math.random();
				if (!m.Inid.contains(o.id) && rndIn < disIn) {
					m.Inid.add(o.id);
					o.Outid.add(m.id);

				}
				if (!m.Outid.contains(o.id) && rndOut < disIn) {
					m.Outid.add(o.id);
					o.Inid.add(m.id);
				}

			}
		}

		mega.clear();
		other.clear();

	}

	private void RandomConnect() {
		// TODO 自動生成されたメソッド・スタブ
		for (Virtual bank : agent) {
			for (int i = 0; i < agent.size(); i++) {
				double rand = Math.random();
				if (bank.id != agent.get(i).id && !bank.Inid.contains(agent.get(i).id) && rand < randIn) {
					bank.Inid.add(agent.get(i).id);
					agent.get(i).Outid.add(bank.id);

					}
			}
		}
		for (Virtual bank : agent) {
			for (int i = 0; i < agent.size(); i++) {
				double rand = Math.random();
				if (bank.id != agent.get(i).id && !bank.Outid.contains(agent.get(i).id) && rand < randOut) {
					bank.Outid.add(agent.get(i).id);
					agent.get(i).Inid.add(bank.id);
				}
			}
		}
	}



	private void makeCluster(ArrayList<Virtual> list) {
		// TODO 自動生成されたメソッド・スタブ



		//listの先頭からminbCNまでを完全ネットワークに
		Collections.shuffle(list);
		for (int i = 0; i < minibCN; i++) {
			for (int j = 0; j < minibCN; j++) {
				if (list.get(j).id != list.get(i).id) {
					list.get(i).Inid.add(list.get(j).id);
					list.get(i).Outid.add(list.get(j).id);
				}
			}
		}

	}

	public int getIndegree(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Inid.size();
	}

	public int getOutdegree(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Outid.size();
	}

	public ArrayList<Integer> getInid(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Inid;
	}

	public ArrayList<Integer> getOutid(int i) {
		// TODO 自動生成されたメソッド・スタブ
		return agent.get(i).Outid;
	}

	public void virtualClear() {
		agent.clear();
	}

}
