package NetworkBA_TD;

import java.util.ArrayList;
import java.util.Iterator;

public class Virtual_BA {

	public ArrayList<Integer> inid = new ArrayList<Integer>();
	public ArrayList<Integer> outid = new ArrayList<Integer>();

	public ArrayList<Integer> new_inid = new ArrayList<Integer>();
	public ArrayList<Integer> new_outid = new ArrayList<Integer>();

	public int indeg_ba,outdeg_ba,sumdeg_ba;
	public int id, newid;



	public Virtual_BA(int i, ArrayList<Integer> inid, ArrayList<Integer> outid) {
		// TODO 自動生成されたコンストラクター・スタブ

		delete_zero_index(inid,outid);

		this.indeg_ba = inid.size();
		this.outdeg_ba = outid.size();
		this.inid.addAll(inid);
		this.outid.addAll(outid);
		sumdeg_ba = indeg_ba + outdeg_ba;
		this.id = i;
		
	}


	private void delete_zero_index(ArrayList<Integer> Inid, ArrayList<Integer> Outid) {
		// TODO 自動生成されたメソッド・スタブ
			Iterator<Integer> in = Inid.iterator();
			while (in.hasNext()) {
				Integer id = in.next();
				if (id == 0)
					in.remove();
			}
			Iterator<Integer> out = Outid.iterator();
			while (out.hasNext()) {
				Integer id = out.next();
				if (id == 0)
					out.remove();
			}

	}

	public int getSumDeg() {
		return sumdeg_ba;
	}


	public ArrayList<Integer> getInid() {
		// TODO 自動生成されたメソッド・スタブ
		return inid;
	}


	public ArrayList<Integer> getOutid() {
		// TODO 自動生成されたメソッド・スタブ
		return outid;
	}


	public ArrayList<Integer> get_new_Inid() {
		// TODO 自動生成されたメソッド・スタブ
		return new_inid;
	}


	public ArrayList<Integer> get_new_Outid() {
		// TODO 自動生成されたメソッド・スタブ
		return new_outid;
	}

}


