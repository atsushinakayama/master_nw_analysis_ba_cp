package NetworkBA_TD;


import java.util.ArrayList;
import java.util.BitSet;

import bankagent.Institution;
import calculator.Utility;

public class Network{

	public Matrix matrix;
	public int order;
	public double theta;

	//コンストラクタ
	public Network(int order,double theta){
		matrix=new Matrix(order,order,false);//マトリクス作成，行列(i,j)
		this.order=order;
		this.theta=theta;
		}


	public Network(int order) {
		// TODO 自動生成されたコンストラクター・スタブ
		matrix = new Matrix(order, order, false);
		this.order = order;
	}


	public void createBA(){//BAグラフの作成（完全グラフ⇒BAグラフ)※これ閾値モデル

		Network complete = new Network(order,theta);//ノード数m(order=150)の完全グラフ
		complete.createComplete();//Order×Order行列作成(BitSety⇒true)

		//完全グラフの作成
		for(int i=0;i<complete.order;i++)
			for(int j=0;j<complete.order;j++)
				matrix.set(i, j, (boolean)complete.matrix.get(i,j));



		//重み付きグラフの作成
		ArrayList<Institution> FWL=new ArrayList<Institution>();
		double tau1[]=new double[order];
		double tau2[]=new double[order];
		double lambda1=0.85;
		double lambda2=0.85;


		for(int i=0;i<order;i++){
			tau2[i]=-Math.log(1-Math.random())/lambda1;
			tau1[i]=-Math.log(1-Math.random())/lambda2;;
		}
		java.util.Arrays.sort(tau1);	//昇順で重みを作成
		java.util.Arrays.sort(tau2);

		for(int i=0;i<order;i++){
			Institution FW=new Institution(i,1,1);//便宜的にindeg,outdegは１としている
			FW.w1=tau1[order-1-i];
			FW.w2=tau2[order-1-i];
			FWL.add(FW);
		}

		//重みの和が閾値より小さいなら線を消す
		for(int i=0;i<FWL.size();i++){
			for(int j=0;j<FWL.size();j++){
				if(i!=j&&FWL.get(i).w1+FWL.get(j).w2<theta){
					matrix.set(i, j,false);
				}
			}
		}

		//ランダムに枝を追加
		for(int i=0;i<order;i++){
			if(getIndegree(i)==0)
				randomInset(i);
			if(getOutdegree(i)==0)
				randomOutset(i);
			}

		}

	public void createCM() {
		// TODO Auto-generated method stub
		Network complete = new Network(order,theta);//ノード数m(order=150)の完全グラフ
		complete.createComplete();
		for(int i=0;i<complete.order;i++)
			for(int j=0;j<complete.order;j++)
				matrix.set(i, j, (boolean)complete.matrix.get(i,j));
		show();
	}





	public void createComplete() {
		// TODO 自動生成されたメソッド・スタブ
		createregular(order/2);

	}


	public void createregular(int k) {//
		// TODO 自動生成されたメソッド・スタブ
		createCircle();

		for(int i=2;i<=k;i++){
			for(int j=0;j<order;j++){
				this.matrix.set(j,(j+i)%order);
				this.matrix.set((j+i)%order, j);
			}
		}
	}


	public void createCircle() {
		// TODO 自動生成されたメソッド・スタブ
		clear();//すべての要素の解除→Matrix.set

		int i=0;
		for(;i<order-1;i++){
			this.matrix.set(i, i+1);//
			this.matrix.set(i+1, i);
		}
		this.matrix.set(i, 0);
		this.matrix.set(0, i);

	}


	public void randomInset(int i) {//入字数をランダムな所に接続
		// TODO Auto-generated method stub
		int j=0;
		do{
			j=(int)(Math.random()*order);
		}while(i!=j);
		matrix.set(j,i,true);
		}


	public void randomOutset(int i) {
		// TODO Auto-generated method stub
		int j=0;
		do{
			j=(int)(Math.random()*order);
		}while(i!=j);
		matrix.set(i,j,true);
		}

	public int getIndegree(int node) {//ノードの出自数を返す
		// TODO Auto-generated method stub
		return matrix.cardinality2(node);
	}

	public int getOutdegree(int node) {//nodeの出次数を返す（行）
		// TODO Auto-generated method stub
		return matrix.cardinality(node);
	}
////////////////////BA&KE2モデル(By Hashimoto,2016,Master_thesis)////////////////////////////////////////////////////////////////////////////


	public void createKE2(int m0, int m, int a, int p){//m0:初期完全グラフノード数，m:追加するノードの枝sの数(ほんま?)，a:だいたい5, p:0だとba?(多分違う)
		//if(m0 < m) throw new IllegalArgumentException("Network : createKE2");

		Network complete = new Network(m0);//ノード数m0の完全グラフ
		complete.createComplete();

		for(int i = 0; i < complete.order; i++)//complete.order=m0=4�i����́j
			for(int j = 0; j < complete.order; j++)
					matrix.set(i, j, (boolean)complete.matrix.get(i, j));//boolean:�^�U��\���^

		int counter = m;
		/*
		matrix.set(0,1,false);
		matrix.set(0,3,false);
		matrix.set(1,2,false);
		matrix.set(2,0,false);
		matrix.set(2,3,false);
		matrix.set(3,1,false);
		*/

		int index;
		BitSet state = new BitSet();
		state.set(0, order);//0からorderまでのbitをtrueに
		//System.out.println(order);

		for(int i = m0; i < order; i++){//order数になるまで繰り返し
			//System.out.println(i);
			if(Utility.MyRandom.nextInt(100) < p){
				int[] degree, nol;
					index = -1;
					degree = new int[state.get(0, i).cardinality()];
					for(int j = 0; j < degree.length; j++) degree[j] = getDegree(index = state.nextSetBit(index+1));
					nol = Utility.MyRandom.nolRoulette(degree, m);

					for(int j = 0; j < nol.length; j++){
						index = -1;
						for(int k = 0; k <= nol[j]; k++)
							index = state.nextSetBit(index+1);
							if(Math.random()<0.5){//�Ώ̂ȗאڍs��ɂȂ�Ȃ��悤�ɂ��ėL���O���t��\��
								matrix.set(i, index);//nextSetBit:�w�肳�ꂽ�J�n�C���f�b�N�X�C�܂��͂��̃C���f�b�N�X��ɐ�����true�ɐݒ肳�ꂽ�ŏ��̃r�b�g�̃C���f�b�N�X��Ԃ�
								//System.out.println("a");
							}
							else{
								matrix.set(index, i);
								//System.out.println("b");
							}
						matrix.set(i, index);
						matrix.set(index, i);
					}
			}else{//BAモデル
				int rnd, cardinality; double sum; double[] pp;
					index = -1;
					cardinality = state.get(0, i).cardinality();//このBitSetの0(を含む)<iまで構成されるBitSetのTrueの数を返す

					for(int j = 0; j < cardinality; j++){

						/*matrix.set(i, index = state.nextSetBit(index+1));
						//nextSetBit:�w�肳�ꂽ�J�n�C���f�b�N�X�C�܂��͂��̃C���f�b�N�X��ɐ�����true�ɐݒ肳�ꂽ�ŏ��̃r�b�g�̃C���f�b�N�X��Ԃ�
						//System.out.println(i+":"+index);
						matrix.set(index, i);*/

						if(Math.random()<0.5){//�Ώ̂ȗאڍs��ɂȂ�Ȃ��悤�ɂ��ėL���O���t��\��
							matrix.set(i, index = state.nextSetBit(index+1));//nextSetBit:�w�肳�ꂽ�J�n�C���f�b�N�X�C�܂��͂��̃C���f�b�N�X��ɐ�����true�ɐݒ肳�ꂽ�ŏ��̃r�b�g�̃C���f�b�N�X��Ԃ�
							//System.out.println("c");
						}
						else{
							matrix.set(index= state.nextSetBit(index+1), i);
							//System.out.println("d");
						}
						//System.out.println(i+":"+index);
					}

					int[] degree = new int[cardinality+1];

					index = -1; sum = 0;
					for(int j = 0; j < cardinality+1; j++)
						sum += (double)1/(degree[j] = getDegree(index = state.nextSetBit(index+1))+a);

					index = -1; pp = new double[cardinality+1];
					for(int j = 0; j < cardinality+1; j++)
						pp[j] = 1/((getDegree(index = state.nextSetBit(index+1))+a)*sum);

					index = -1; rnd = Utility.MyRandom.roulette(pp);
					for(int j = 0; j <= rnd; j++) index = state.nextSetBit(index+1);
					state.set(index, false);
			}
		}
	}

	public int[] getDegree(){
		int[] degree = new int[order];

		for(int i = 0; i < order; i++)
			degree[i] = getDegree(i);

		return degree;
	}

	public int getDegree(int node){//node�̎�����Ԃ��i�������Əo�����̘a�j
		return matrix.cardinality(node)+matrix.cardinality2(node);
	}



///////////////////金融機関バランスシートの構成////////////////////////////

	public ArrayList<Integer> getInid(int node) {//入ってきている金融機関のid配列
		// TODO Auto-generated method stub
		return matrix.setInid(node);
	}

	public ArrayList<Integer> getOutid(int node) {
		// TODO Auto-generated method stub
		return matrix.setOutid(node);
	}


	public void clear() {
		// TODO 自動生成されたメソッド・スタブ
		matrix.clear();
}

	public void show() {
		// TODO Auto-generated method stub
		matrix.show();
	}






}
