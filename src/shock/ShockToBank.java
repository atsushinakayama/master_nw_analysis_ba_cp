package shock;

import java.util.ArrayList;
import java.util.Collections;

import bankagent.Institution;
import comparator.GetBBComparator;
import firmagent.CandidateBank;

public class ShockToBank {

	public ArrayList<CandidateBank> ffbb = new ArrayList<CandidateBank>();

	public ShockToBank(ArrayList<CandidateBank> candidatelist,ArrayList<Institution> bank){
		ffbb.addAll(candidatelist);
		setfbbrank(bank);
	}

	public void setfbbrank(ArrayList<Institution> bank) {
		ffbb.stream().forEach(s->{
			for(Institution b:bank) {
				if(s.id==b.id) {
					s.rank=b.rank;
				}
			}
		});
	}

	public void setdescBorrowingAmount() {
		// TODO 自動生成されたメソッド・スタブ
		Collections.sort(ffbb, new GetBBComparator());
	}


}
