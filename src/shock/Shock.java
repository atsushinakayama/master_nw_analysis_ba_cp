package shock;

import java.util.ArrayList;
import java.util.Collections;

import asset.Asset;
import bankagent.Institution;
import firmagent.Enterprise;

public class Shock {


	public int shockterm;
	public ArrayList<ShockToBank> shocktobankbyfirm =new ArrayList<ShockToBank>();

	public Shock(int shockterm) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.shockterm=shockterm;
	}

	public void createAssetGroupShock() {
		// TODO 自動生成されたメソッド・スタブ
	}

	public void createAssetShock(ArrayList<Asset> asset, double dec, String target) {
		// TODO 自動生成されたメソッド・スタブ
		//下落させるassetの決定
		switch(target) {
		case "MOST":
			int id=0;double counter=0;
			for(Asset as:asset) {
				if(counter<as.totalamount) {
					counter=as.totalamount;
					id=as.id;
				}
			}
			for(Asset as:asset) {
				if(as.id==id) {
					as.dec=dec;
					//System.out.println(as.id+"id assetshock");
				}
			}
			break;
		case "RANDOM":
			Collections.shuffle(asset);
			asset.get(0).dec=dec;
			break;
		default:
			System.out.println("not choiced shock asset");
		}
	}

/////// to assign same asset shock func. ////////////////////////////////////////////
	public int createAssetShock1(ArrayList<Asset> asset, double dec, String target) {
		// TODO 自動生成されたメソッド・スタブ
		int shockid=0;
		switch(target) {
		case "MOST":
			int id=0;double counter=0;
			for(Asset as:asset) {
				if(counter<as.totalamount) {
					counter=as.totalamount;
					id=as.id;
				}
			}
			for(Asset as:asset) {
				if(as.id==id) {
					as.dec=dec;
					shockid = as.id;
					System.out.println("shockid "+as.id);
				}
			}
			break;
		case "RANDOM":
			Collections.shuffle(asset);
			asset.get(0).dec=dec;
			shockid = asset.get(0).id;
			break;
		default:
			System.out.println("not choiced shock asset");
		}
		return shockid;
	}

	public void assignAssetShock(ArrayList<Asset> asset, double dec, int shockid) {
		// TODO 自動生成されたメソッド・スタブ

		for(Asset as:asset) {
			if(as.id == shockid) {
				as.dec = dec;
				System.out.println("shockid "+as.id);
			}
		}

	}

///////////////////////////////////////////////////////////////////

	public void createLargestFirmShock(ArrayList<Enterprise> firm,ArrayList<Institution> bank) {
		// TODO 自動生成されたメソッド・スタブ
		if(shocktobankbyfirm.size()>0) shocktobankbyfirm.clear();
		firm.get(0).state="Dead";
		ShockToBank stb = new ShockToBank(firm.get(0).Candidatelist,bank);
		shocktobankbyfirm.add(stb);
		stb.setdescBorrowingAmount();
	}

	public void createLargestBankShock(ArrayList<Institution> bank) {
		// TODO 自動生成されたメソッド・スタブ
		bank.get(0).state="Dead";
		bank.get(0).brfactor="CAR";
	}

	public ArrayList<ShockToBank> getShocktobanklist() {
		// TODO 自動生成されたメソッド・スタブ
		return shocktobankbyfirm;
	}

	public void createMiddleFirmShock(ArrayList<Enterprise> firm, ArrayList<Institution> bank) {
		// TODO 自動生成されたメソッド・スタブ
		if(shocktobankbyfirm.size()>0) shocktobankbyfirm.clear();
		firm.get(499).state="Dead";
		ShockToBank stb = new ShockToBank(firm.get(0).Candidatelist,bank);
		shocktobankbyfirm.add(stb);
		stb.setdescBorrowingAmount();
	}







}
