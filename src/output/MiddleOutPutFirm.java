package output;

import firmagent.Enterprise;

public class MiddleOutPutFirm {

	public int i,atp;


	public MiddleOutPutFirm(int i, int atp) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.i=i;
		this.atp=atp;
	}

	public double avgk;
	public double avgl;
	public double avgn;
	public double avglev;
	public double avgreallev;
	public double avgdeg;
	public double avgmaxmainbank;


	public void setResultAndCalc(Enterprise f) {

			avgk+=f.K/atp;
			avgl+=f.l/atp;
			avgn+=f.A/atp;
			avglev+=f.lev/atp;
			avgreallev+=f.reallev/atp;
			avgdeg+=(double)f.mainbanknum/atp;
			avgmaxmainbank+=(double)f.maxmainbank/atp;



	}

}
