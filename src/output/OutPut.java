package output;
import java.util.ArrayList;

import bankagent.Institution;
import firmagent.Enterprise;

public class OutPut implements Cloneable{

	/*
	 * 各試行ごとのの結果をリストに格納
	 * output.size()==atp
	 * リストget(x)⇒termごとの結果
	 */

	public int atp;//atp試行目
	public int term;

	public OutPut(int i,int term) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.atp=i;
		this.term=term;
	}

	//各ターム出力結果
	public ArrayList<Double> Grossproduction=new ArrayList<Double>();
	public ArrayList<Double> Productgrowthrate=new ArrayList<Double>();
	public ArrayList<Integer> FFnum=new ArrayList<Integer>();
	public ArrayList<Double> Avginterest=new ArrayList<Double>();
	public ArrayList<Integer> Rejectfinancing=new ArrayList<Integer>();


	//企業（平均)
	public ArrayList<Double> Avgfirmlev=new ArrayList<Double>();

	//銀行（平均）
	public ArrayList<Double> Avgbankclientnum=new ArrayList<Double>();
	public ArrayList<Double> Grossbankprofit=new ArrayList<Double>();
	public ArrayList<Double> Grossbankbuffer=new ArrayList<Double>();

	//エージェント
	public ArrayList<Institution> Finalbank=new ArrayList<Institution>();
	public ArrayList<Enterprise> Finalfirm=new ArrayList<Enterprise>();

	public ArrayList<Institution> Initbank=new ArrayList<Institution>();

	public int getBanksize() {
		// TODO 自動生成されたメソッド・スタブ
		return Finalbank.size();
	}

	public int getFirmSize() {
		return Finalfirm.size();
	}

	public OutPut clone() {
		try {
			return (OutPut) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());

		}
	}



}
