package output;

import java.util.ArrayList;

import bankagent.Institution;

public class FinalOutPutShort {

	public int atp;
	public int expterm;
	public int shortterm;
	public int grossterm;

	public ArrayList<Double> AvgFailedBankNum = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankBoth = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankCAR = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankGap = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankCM = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankMega = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankMedium = new ArrayList<Double>();
	public ArrayList<Double> AvgFailedBankSmall = new ArrayList<Double>();
	public ArrayList<Double> AvgNotRollOver = new ArrayList<Double>();
	public ArrayList<Double> AvgIBSupply = new ArrayList<Double>();
	public ArrayList<Double> AvgAliveBankNum = new ArrayList<Double>();
	public ArrayList<Double> AvgDebtRank = new ArrayList<Double>();
	//nw
	public ArrayList<Double> DifPageRank = new ArrayList<Double>();
	public ArrayList<Double> MaxDifPageRank = new ArrayList<Double>();
	public ArrayList<Double> MinDifPageRank = new ArrayList<Double>();

	public ArrayList<Double> TotalLossesToBank = new ArrayList<Double>();

	public FinalOutPutShort(int atp, int expterm, int shortterm) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.atp = atp;
		this.expterm = expterm;
		this.shortterm = shortterm;
		this.grossterm = expterm * shortterm;
	}

	public void Calc(ArrayList<OutPutShort> outputshort) {

		//各タームoutput
		int a = 0, b = 0, c = 0, d = 0, e = 0, h = 0, l = 0, m = 0, n = 0;
		double f = 0, g = 0, o = 0;
		for (int i = 0; i < grossterm; i++) {
			for (OutPutShort ops : outputshort) {
				a += ops.FailedBankNum.get(i);
				b += ops.FailedBankBoth.get(i);
				c += ops.FailedBankCAR.get(i);
				d += ops.FailedBankGap.get(i);
				f += ops.NotRollOver.get(i);
				g += ops.IBSupply.get(i);
				h += ops.AliveBankNum.get(i);
				l += ops.FailedBankMega.get(i);
				m += ops.FailedBankMedium.get(i);
				n += ops.FailedBankSmall.get(i);
				o += ops.AvgDebtRank.get(i);
			}
			AvgFailedBankNum.add((double) a / atp);
			AvgFailedBankBoth.add((double) b / atp);
			AvgFailedBankCAR.add((double) c / atp);
			AvgFailedBankGap.add((double) d / atp);
			AvgNotRollOver.add((double) f / atp);
			AvgIBSupply.add((double) g / atp);
			AvgAliveBankNum.add((double) h / atp);
			AvgFailedBankMega.add((double) l / atp);
			AvgFailedBankMedium.add((double) m / atp);
			AvgFailedBankSmall.add((double) n / atp);
			AvgDebtRank.add((double) o / atp);
			a = 0;
			b = 0;
			c = 0;
			d = 0;
			f = 0.0;
			g = 0.0;
			h = 0;
			l = 0;
			m = 0;
			n = 0;
			o = 0.0;
		}
		for (int i = 0; i < expterm; i++) {
			for (OutPutShort ops : outputshort) {
				e += ops.FailedBankCM.get(i);
			}
			AvgFailedBankCM.add((double) e / atp);
			e = 0;
		}

		// 各試行 output
		for(OutPutShort ops:outputshort) {
			double loss=0;
			for(Institution bank:ops.FailedBank) {
				loss += bank.bb;
			}
			TotalLossesToBank.add(loss);
		}




	}

}
