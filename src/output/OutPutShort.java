package output;

import java.util.ArrayList;

import bankagent.Institution;

public class OutPutShort {

	public int atp;
	public int expterm;//longterm
	public int shortterm;//shortterm

	public OutPutShort(int i, int expterm, int shortterm) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.atp=i;
		this.expterm=expterm;
		this.shortterm=shortterm;
	}

	//各ショートターム出力
	public ArrayList<Integer> FailedBankNum=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankBoth=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankCAR=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankGap=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankCM=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankMega=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankMedium=new ArrayList<Integer>();
	public ArrayList<Integer> FailedBankSmall=new ArrayList<Integer>();
	public ArrayList<Double> NotRollOver=new ArrayList<Double>();
	public ArrayList<Double> IBSupply=new ArrayList<Double>();
	public ArrayList<Integer> AliveBankNum=new ArrayList<Integer>();
	public ArrayList<Double> DifPageRank=new ArrayList<Double>();
	public ArrayList<Double> MaxDifPageRank=new ArrayList<Double>();
	public ArrayList<Double> MinDifPageRank=new ArrayList<Double>();
	public ArrayList<Double> AvgDebtRank=new ArrayList<Double>();

	//ショートターム全体出力
	public ArrayList<Institution> FailedBank = new ArrayList<Institution>();
	public int failed_mega, failed_medium, failed_small;

	public void setFailed_Bank_by_size(ArrayList<Institution> deadbank) {

		for(Institution fb:deadbank) {
			if(fb.rank == "mega") {
				failed_mega++;
			}
			if(fb.rank == "medium") {
				failed_medium++;
			}
			if(fb.rank == "small") {
				failed_small++;
			}
		}
	}



}

