package comparator;

import java.util.Comparator;

import NetworkBA_TD.Virtual_BA;

public class Bank_Sumdeg_BA_Comp implements Comparator<Virtual_BA>{

			public int compare(Virtual_BA o1, Virtual_BA o2){
				int no1=o1.sumdeg_ba;
				int no2=o2.sumdeg_ba;
				if(no1<no2)return 1;
				if(no1==no2) {return 0;}else{return -1;}
			}
}
